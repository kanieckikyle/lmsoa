from django.contrib import admin

from events.models import CalendarEvent

# Register your models here.

class CalendarEventConsole(admin.ModelAdmin):
    list_display = ('id', 'organizer', 'start_date', 'end_date', 'frequency')

admin.site.register(CalendarEvent, CalendarEventConsole)
