# Generated by Django 2.1.4 on 2019-01-30 16:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0006_auto_20190129_2013'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='calendarevent',
            name='interval',
        ),
        migrations.AlterField(
            model_name='calendarevent',
            name='end_date',
            field=models.DateTimeField(blank=True, null=True),
        ),
    ]
