from django.db import models
import logging
from dateutil.relativedelta import relativedelta
import calendar
from dateutil.rrule import rrule, DAILY, MONTHLY, WEEKLY, MONTHLY, YEARLY

from accounts.models import Owner

logger = logging.getLogger(__name__)

class CalendarEventManager(models.Manager):

    def get_recurrent_events(self):
        return super().get_queryset().filter(frequency__gte=0)

    def get_events_for_month(self, date):
        first_day = date + relativedelta(day=1)
        first_day.replace(hour=0, minute=0)

        last_day = date + relativedelta(day=1, months=+1, days=-1)
        last_day.replace(hour=11, minute=59)

        return self.get_events_between_dates(first_day, last_day)

    def get_events_for_week(self, date):
        first_day = date + relativedelta(weekday=calendar.SUNDAY, weeks=-1)
        first_day = first_day.replace(hour=0, minute=0)

        last_day = first_day + relativedelta(weekday=calendar.SATURDAY)
        last_day = last_day.replace(hour=11, minute=59)

        events = self.get_events_between_dates(first_day, last_day)
        return events

    def get_events_for_day(self, date):
        last_hour = date.replace(hour=11, minute=59)
        first_hour = date.replace(hour=0, minute=0)

        return self.get_events_between_dates(first_hour, last_hour)

    def get_events_between_dates(self, start_date, end_date):
        events = []
        for event in self.get_recurrent_events():
            dates = rrule(
                event.frequency,
                until=end_date,
                dtstart=event.start_date)
            dates = dates.between(start_date, end_date, inc=False)

            dates = map(lambda date: CalendarEvent(
                id=event.id,
                organizer=event.organizer,
                title=event.title,
                description=event.description,
                start_date=date,
                end_date=date + (event.end_date - event.start_date),
                frequency=event.frequency,
            ), dates)
            events += list(dates)

        events += list(super().get_queryset().filter(start_date__gte=start_date, end_date__lte=end_date, frequency=-1))
        return events


class CalendarEvent(models.Model):

    RECURRENCE_CHOICES = (
        (-1, 'None'),
        (DAILY, 'Daily'),
        (WEEKLY, 'Weekly'),
        (MONTHLY, 'Monthly'),
        (YEARLY, 'Yearly')
    )

    organizer = models.ForeignKey(Owner, on_delete=models.CASCADE, related_name='events')

    start_date = models.DateTimeField(null=False, blank=False)

    end_date = models.DateTimeField(null=True, blank=True)

    title = models.CharField(max_length=50, null=False, blank=False)

    description = models.TextField()

    frequency = models.IntegerField(choices=RECURRENCE_CHOICES, null=False, blank=False, default=-1)

    objects = CalendarEventManager()
