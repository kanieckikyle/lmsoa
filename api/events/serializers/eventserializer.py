from rest_framework import serializers

from events.models import CalendarEvent


class CalendarEventSerializer(serializers.ModelSerializer):

    class Meta:
        model = CalendarEvent
        fields = (
            'id',
            'organizer',
            'start_date',
            'end_date',
            'title',
            'description',
            'frequency',
        )
