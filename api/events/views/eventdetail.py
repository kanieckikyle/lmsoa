from rest_framework import permissions, status
from rest_framework.views import APIView, Response

from events.models import CalendarEvent
from events.serializers import CalendarEventSerializer


class CalendarEventDetail(APIView):

    def get(self, request, pk, format=None):
        try:
            event = CalendarEvent.objects.get(id=pk)
        except CalendarEvent.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        serializer = CalendarEventSerializer(event)
        return Response(data=serializer.data, status=status.HTTP_200_OK)

    def patch(self, request, pk, format=None):
        try:
            event = CalendarEvent.objects.get(id=pk)
        except CalendarEvent.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        serializer = CalendarEventSerializer(instance=event, data=request.data)

        if serializer.is_valid():
            event = serializer.update(event, serializer.validated_data)
            return Response(data=CalendarEventSerializer(event).data, status=status.HTTP_200_OK)

        return Response(data=serializer.errors, status=status.HTTP_200_OK)
