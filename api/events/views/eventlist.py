from rest_framework import permissions, status
from rest_framework.views import APIView, Response
from django.utils import timezone
from dateutil import parser
import pytz

from events.models import CalendarEvent
from events.serializers import CalendarEventSerializer

class CalendarEventList(APIView):

    def get(self, request, format=None):
        range = request.GET.get('range', 'weekly')

        if 'start_date' in request.GET and 'end_date' in request.GET:
            start_date = parser.parse(request.GET['start_date']).replace(tzinfo=pytz.UTC)
            end_date = parser.parse(request.GET['end_date']).replace(tzinfo=pytz.UTC)
            events = CalendarEvent.objects.get_events_between_dates(start_date, end_date)
        elif range == 'month':
            events = CalendarEvent.objects.get_events_for_month(timezone.now())
        elif range == 'day':
            events = CalendarEvent.objects.get_events_for_day(timezone.now())
        else:
            events = CalendarEvent.objects.get_events_for_week(timezone.now())

        serializer = CalendarEventSerializer(events, many=True)

        return Response(data=serializer.data, status=status.HTTP_200_OK)

    def post(self, request, format=None):

        serializer = CalendarEventSerializer(data=request.data)

        if serializer.is_valid():
            event = serializer.create(serializer.validated_data)
            return Response(CalendarEventSerializer(event).data, status=status.HTTP_200_OK)

        return Response(data=serializer.errors, status=status.HTTP_400_BAD_REQUEST)
