from .eventlist import CalendarEventList
from .eventdetail import CalendarEventDetail

__all__ = [
    'CalendarEventList',
    'CalendarEventDetail'
]
