from django.urls import path

from .views import CalendarEventList, CalendarEventDetail

urlpatterns = [
    path('events/', CalendarEventList.as_view()),
    path('events/<int:pk>/', CalendarEventDetail.as_view())
]
