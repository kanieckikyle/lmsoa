from django.urls import path

from lmsoamessages.views import \
    MessageListView, \
    MessageDetailView, \
    CommentListView, \
    CommentDetailView

urlpatterns = [
    path('messages/', MessageListView.as_view(), name='MessageList'),
    path('messages/<int:pk>/', MessageDetailView.as_view(), name='MessageDetail'),
    path('messages/<int:pk>/comments/', CommentListView.as_view(), name='CommentList'),
    path('messages/<int:pk>/comments/<int:comment_id>/', CommentDetailView.as_view(), name='CommentDetail')
]
