from rest_framework import serializers

from lmsoamessages.models import Message
from lmsoamessages.serializers import CommentSerializer
from accounts.serializers import ShortOwnerSerializer

class MessageSerializer(serializers.ModelSerializer):

    author = ShortOwnerSerializer(read_only=True)
    comments = CommentSerializer(many=True, read_only=True)

    class Meta:
        model = Message
        fields = (
            'id',
            'author',
            'title',
            'body',
            'pinned',
            'views',
            'timestamp',
            'comments'
        )

        read_only_fields = ('id', 'timestamp', 'views', 'comments')

    def create(self, validated_data, request):
        validated_data['author'] = request.user
        message = super().create(validated_data)
        if not request.user.is_superuser:
            message.pinned = False

        message.save()
        return message

    def update(self, instance, validated_data):
        if 'author' in validated_data:
            del validated_data['author']
        message = super().update(instance, validated_data)

        return message
