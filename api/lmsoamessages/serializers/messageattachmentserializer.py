from rest_framework import serializers

from lmsoamessages.models import MessageAttachment


class MessageAttachmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = MessageAttachment
        fields = (
            'id',
            'file'
        )
