from rest_framework import serializers

from lmsoamessages.models import Comment, Message
from accounts.serializers import ShortOwnerSerializer


class CommentSerializer(serializers.ModelSerializer):

    author = ShortOwnerSerializer(read_only=True)

    class Meta:
        model = Comment
        fields = (
            'id',
            'message',
            'author',
            'body',
            'timestamp'
        )

        read_only_fields = ('id', 'author', 'message')

    def create(self, validated_data, request, message_id):
        validated_data['author'] = request.user
        validated_data['message'] = Message.objects.get(id=message_id)
        comment = super().create(validated_data)
        return comment
