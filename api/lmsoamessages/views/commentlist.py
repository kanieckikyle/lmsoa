from rest_framework.views import APIView, Response
from rest_framework import status, permissions

from lmsoamessages.models import Comment, Message
from lmsoamessages.serializers import CommentSerializer


class CommentListView(APIView):

    def get(self, request, pk, format=None):
        comments = Comment.objects.filter(message__id=pk)
        serializer = CommentSerializer(instance=comments, many=True)

        return Response(data=serializer.data, status=status.HTTP_200_OK)

    def post(self, request, pk, format=None):
        serializer = CommentSerializer(data=request.data)

        if serializer.is_valid():
            comment = serializer.create(serializer.validated_data, request, pk)
            return Response(data=CommentSerializer(comment).data, status=status.HTTP_200_OK)

        return Response(data=serializer.errors, status=status.HTTP_400_BAD_REQUEST)
