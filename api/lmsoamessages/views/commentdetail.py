from rest_framework.views import APIView, Response
from rest_framework import status, permissions

from lmsoamessages.models import Comment, Message
from lmsoamessages.serializers import CommentSerializer


class CommentDetailView(APIView):

    # TODO: Permissions need to be updated

    def get(self, request, pk, comment_id, format=None):
        comments = Comment.objects.filter(message__id=pk)
        serializer = CommentSerializer(instance=comments, many=True)

        return Response(data=serializer.data, status=status.HTTP_200_OK)

    def patch(self, request, pk, comment_id, format=None):
        try:
            comment = Comment.objects.get(id=comment_id)
        except Comment.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        serializer = CommentSerializer(instance=comment, data=request.data)

        if serializer.is_valid():
            comment = serializer.update(comment, serializer.validated_data)
            return Response(data=CommentSerializer(comment).data, status=status.HTTP_200_OK)

        return Response(data=serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, comment_id, format=None):
        try:
            comment = Comment.objects.get(id=comment_id)
        except Comment.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        comment.delete()

        return Response(status=status.HTTP_204_NO_CONTENT)
