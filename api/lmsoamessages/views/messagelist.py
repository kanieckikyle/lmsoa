from rest_framework.views import APIView, Response
from rest_framework import status, permissions
from django.db.models import Q

from lmsoamessages.models import Message
from lmsoamessages.serializers import MessageSerializer
from django.core.paginator import Paginator, InvalidPage, EmptyPage, Page


class MessageListView(APIView):

    def get(self, request, format=None):
        messages = self.get_queryset(
            request,
            Message.objects.prefetch_related('comments', 'attachments').all()
        )

        page = request.GET.get('page', 1)
        page_length = request.GET.get('page_length', 15)

        paginator = Paginator(list(messages), page_length)
        try:
            p_page = paginator.page(page)
        except (InvalidPage, EmptyPage) as e:
            p_page = Page([], page, paginator)

        data = {
            'page': int(page),
            'page_length': page_length,
            'items': MessageSerializer(p_page.object_list, many=True).data
        }

        return Response(data=data, status=status.HTTP_200_OK)

    def post(self, request, format=None):
        serializer = MessageSerializer(data=request.data)

        if serializer.is_valid():
            message = serializer.create(serializer.validated_data, request)
            return Response(data=MessageSerializer(instance=message).data, status=status.HTTP_200_OK)

        return Response(data=serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self, request, queryset):
        pinned = request.GET.get('pinned', None)
        query = request.GET.get('query', None)

        q = Q()
        if pinned != None:
            q = q & Q(pinned=pinned == 'true')
        if query != None:
            q = q & (Q(author__icontains=query) | Q(title__icontains=query | Q(body__icontains=query)))

        return queryset.filter(q).order_by('-pinned', '-timestamp')
