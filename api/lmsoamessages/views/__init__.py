from .messagelist import MessageListView
from .messagedetail import MessageDetailView
from .commentlist import CommentListView
from .commentdetail import CommentDetailView

__all__ = [
    'MessageListView',
    'MessageDetailView',
    'CommentListView',
    'CommentDetailView'
]
