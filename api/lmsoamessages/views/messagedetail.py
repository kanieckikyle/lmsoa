from rest_framework.views import APIView, Response
from rest_framework import status, permissions

from lmsoamessages.models import Message
from lmsoamessages.serializers import MessageSerializer


class MessageDetailView(APIView):

    def get(self, request, pk, format=None):
        try:
            message = Message.objects.prefetch_related('comments', 'attachments').get(id=pk)
        except Message.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        serializer = MessageSerializer(message)

        return Response(data=serializer.data, status=status.HTTP_200_OK)

    def patch(self, request, pk, format=None):
        try:
            message = Message.objects.prefetch_related('comments', 'attachments').get(id=pk)
        except Message.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        serializer = MessageSerializer(instance=message, data=request.data, partial=True)

        if serializer.is_valid():
            message = serializer.update(instance=message, validated_data=serializer.validated_data)
            return Response(data=MessageSerializer(message).data, status=status.HTTP_200_OK)

        return Response(data=serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        try:
            message = Message.objects.get(id=pk)
        except Message.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        message.delete()

        return Response(status=status.HTTP_200_OK)
