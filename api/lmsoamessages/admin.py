from django.contrib import admin

from lmsoamessages.models import Comment, Message, MessageAttachment

# Register your models here.


class MessageAdminConsole(admin.ModelAdmin):
    list_display = ('id', 'title', 'author', 'pinned')


class CommentAdminConsole(admin.ModelAdmin):
    list_display = ('id', 'message', 'author')


class MessageAttachmentAdminConsole(admin.ModelAdmin):
    list_display = ('id', 'message')

admin.site.register(Message, MessageAdminConsole)
admin.site.register(Comment, CommentAdminConsole)
admin.site.register(MessageAttachment, MessageAttachmentAdminConsole)
