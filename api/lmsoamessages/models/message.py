from django.db import models

from accounts.models import Owner


class Message(models.Model):

    author = models.ForeignKey(Owner, on_delete=models.CASCADE, related_name='messages')

    title = models.CharField(max_length=50, null=False, blank=False)

    body = models.TextField(null=False, blank=False)

    pinned = models.BooleanField(default=False)

    views = models.PositiveIntegerField(default=0)

    timestamp = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title
