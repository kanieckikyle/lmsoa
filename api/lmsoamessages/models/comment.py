from django.db import models

from .message import Message
from accounts.models import Owner


class Comment(models.Model):

    message = models.ForeignKey(Message, on_delete=models.CASCADE, related_name='comments')

    author = models.ForeignKey(Owner, on_delete=models.CASCADE, related_name='author')

    body = models.TextField()

    timestamp = models.DateTimeField(auto_now=True)
