from django.db import models

from .message import Message


class MessageAttachment(models.Model):

    message = models.ForeignKey(Message, on_delete=models.CASCADE, related_name='attachments')

    file = models.FileField()
