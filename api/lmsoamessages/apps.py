from django.apps import AppConfig


class LmsoaMessagesConfig(AppConfig):
    name = 'lmsoamessages'
