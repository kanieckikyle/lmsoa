#!/bin/bash

python3 manage.py migrate        # Apply database migrations
# python3 manage.py collectstatic --clear --noinput # clearstatic files
python3 manage.py collectstatic --noinput  # collect static files

python3 manage.py create_superusers

touch /srv/logs/gunicorn.log

# Start supervisor daemon
/usr/bin/supervisord -c /etc/supervisor/supervisord.conf

echo Starting nginx
nginx -g 'daemon off;'
