from django.urls import path
from classifieds.views import ClassifiedListView, ClassifiedAttachmentListView, ClassifiedDetailView

urlpatterns = [
    path('classifieds/', ClassifiedListView.as_view(), name='ClassifiedList'),
    path('classifieds/<uuid:pk>/attachments/', ClassifiedAttachmentListView.as_view(), name='ClassifiedAttachments'),
    path('classifieds/<uuid:pk>/', ClassifiedDetailView.as_view(), name="ClassifiedDetail")
]
