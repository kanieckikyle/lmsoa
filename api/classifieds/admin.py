from django.contrib import admin

from classifieds.models import MarketplaceListing, RealEstateListing, ListingAttachment

# Register your models here.


class MarketplaceListingConsole(admin.ModelAdmin):
    list_display = ('id', 'author', 'title', 'price', 'public')


class RealEstateListingConsole(admin.ModelAdmin):
    list_display = ('id', 'author', 'title', 'price', 'public')

class ListingAttachmentConsole(admin.ModelAdmin):
    list_display = ('id', 'marketplace_listing', 'realestate_listing')


admin.site.register(RealEstateListing, RealEstateListingConsole)
admin.site.register(MarketplaceListing, MarketplaceListingConsole)
admin.site.register(ListingAttachment, ListingAttachmentConsole)
