from rest_framework import permissions

class ClassifiedListPermissions(permissions.BasePermission):
    """
    Global permission check for user
    """

    def has_permission(self, request, view):
        if request.method == 'GET':
            return True

        return request.user.is_anonymous == False

    def has_object_permission(self, request, view, obj):
        return request.user.is_superuser or request.user == obj.playe
