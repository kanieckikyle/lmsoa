from django.db import models
from .listing import Listing


class RealEstateListing(Listing):

    lot_number = models.PositiveIntegerField(null=False, blank=False)

    type = models.CharField(default='realestate', editable=False, max_length=10)

    @property
    def attachments(self):
        return self.realestate_attachments
