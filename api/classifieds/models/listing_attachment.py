import os
from django.db import models

def get_upload_path(instance, filename):
    return os.path.join("media", "classifieds", filename)


class ListingAttachment(models.Model):

    marketplace_listing = models.ForeignKey('MarketplaceListing', on_delete=models.CASCADE, related_name='marketplace_attachments', null=True, blank=True)

    realestate_listing = models.ForeignKey('RealEstateListing', on_delete=models.CASCADE, related_name='realestate_attachments', null=True, blank=True)

    file = models.ImageField(null=True, blank=True, upload_to=get_upload_path)

    def __str__(self):
        if self.file:
            return self.file.url

        return ''
