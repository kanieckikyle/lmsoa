from django.db import models

from .listing import Listing
from .listing_attachment import ListingAttachment


class MarketplaceListing(Listing):
    type = models.CharField(default='marketplace', editable=False, max_length=11)

    @property
    def attachments(self):
        return self.marketplace_attachments
