from django.db import models
import uuid

from accounts.models import Owner

class Listing(models.Model):

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)

    author = models.ForeignKey(Owner, on_delete=models.CASCADE, related_name='%(class)s_listings', null=False)

    title = models.CharField(max_length=140, null=False, blank=False, default='')

    description = models.TextField(null=False, blank=False, default='')

    price = models.FloatField(null=False, blank=False, default=0.0)

    public = models.BooleanField(default=False, blank=False)

    timestamp = models.DateTimeField(auto_now=True, editable=False)

    class Meta:
        abstract = True
