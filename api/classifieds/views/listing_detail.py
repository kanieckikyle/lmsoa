from rest_framework.views import APIView, Response
from rest_framework import permissions, status
from rest_framework.parsers import JSONParser, MultiPartParser
from django.http import Http404

from classifieds.models import RealEstateListing, MarketplaceListing, ListingAttachment
from classifieds.serializers import RealEstateListingSerializer, MarketplaceListingSerializer
from classifieds.permissions import ClassifiedListPermissions


class ClassifiedDetailView(APIView):

    def get(self, request, pk, format=None):
        pass

    def patch(self, request, pk, format=None):
        pass

    def delete(self, request, pk, format=None):
        listing = self.get_classified(pk)
        listing.delete()
        return Response(status=status.HTTP_200_OK)

    def get_classified(self, pk):
        try:
            return RealEstateListing.objects.get(id=pk)
        except RealEstateListing.DoesNotExist:
            try:
                return MarketplaceListing.objects.get(id=pk)
            except MarketplaceListing.DoesNotExist:
                raise Http404
