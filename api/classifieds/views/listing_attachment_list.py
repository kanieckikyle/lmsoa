from django.http import Http404
from rest_framework.views import APIView, Response
from rest_framework import permissions, status
from rest_framework.parsers import JSONParser, MultiPartParser
from rest_framework.exceptions import ParseError
import re

from classifieds.models import RealEstateListing, MarketplaceListing, ListingAttachment
from classifieds.serializers import RealEstateListingSerializer, MarketplaceListingSerializer
from classifieds.permissions import ClassifiedListPermissions


class ClassifiedAttachmentListView(APIView):

    parser_classes = (MultiPartParser, JSONParser)
    permission_classes = (ClassifiedListPermissions, )

    def get(self, request, pk, format=None):
        listing, serializer = self.get_listing(pk)

        return Response(
            data=[str(attachment) for attachment in listing.attachments.all()],
            status=status.HTTP_200_OK)

    def post(self, request, pk, format=None):
        listing, serializer = self.get_listing(pk)

        att_regex = re.compile('attachment[0-9]+')
        attachments = []
        for key, file in request.data.items():
            if att_regex.match(key):
                if isinstance(listing, RealEstateListing):
                    attachment = ListingAttachment(realestate_listing=listing)
                else:
                    attachment = ListingAttachment(marketplace_listing=listing)
                attachment.save()
                attachment.file.save(file.name, file, save=True)
                attachments.append(attachment)

        return Response(
            data=[str(attachment) for attachment in attachments],
            status=status.HTTP_200_OK)

    def get_listing(self, pk):
        try:
            listing = RealEstateListing.objects.get(id=pk)
            return listing, RealEstateListingSerializer(instance=listing)
        except RealEstateListing.DoesNotExist:
            try:
                listing = MarketplaceListing.objects.get(id=pk)
                return listing, MarketplaceListingSerializer(instance=listing)
            except MarketplaceListing.DoesNotExist:
                raise Http404('A listing with that id does not exist')
