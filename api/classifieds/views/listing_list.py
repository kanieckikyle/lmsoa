from rest_framework.views import APIView, Response
from rest_framework import permissions, status
from rest_framework.parsers import JSONParser, MultiPartParser

from classifieds.models import RealEstateListing, MarketplaceListing, ListingAttachment
from classifieds.serializers import RealEstateListingSerializer, MarketplaceListingSerializer
from classifieds.permissions import ClassifiedListPermissions


class ClassifiedListView(APIView):

    parser_classes = (MultiPartParser, JSONParser)
    permission_classes = (ClassifiedListPermissions, )

    def get(self, request, format=None):
        if request.user.is_anonymous:
            realestate = RealEstateListing.objects.filter(public=True)
            marketplace = MarketplaceListing.objects.filter(public=True)
        else:
            realestate = RealEstateListing.objects.all()
            marketplace = MarketplaceListing.objects.all()
        
        data = RealEstateListingSerializer(realestate, many=True).data + MarketplaceListingSerializer(marketplace, many=True).data

        data.sort(key=lambda x: x['timestamp'], reverse=True)

        return Response(data=data, status=status.HTTP_200_OK)

    def post(self, request, format=None):

        type = request.data.get('type', '')
        if type == 'realestate':
            serializer = RealEstateListingSerializer(data=request.data)
        elif type == 'marketplace':
            serializer = MarketplaceListingSerializer(data=request.data)
        else:
            return Response(data={'errors': ['You must specify a listing type!']}, status=status.HTTP_400_BAD_REQUEST)

        if serializer.is_valid():
            listing = serializer.create(serializer.validated_data, request)
            if type == 'realestate':
                return Response(data=RealEstateListingSerializer(instance=listing).data, status=status.HTTP_200_OK)
            else:
                return Response(data=MarketplaceListingSerializer(instance=listing).data, status=status.HTTP_200_OK)

        return Response(data=serializer.errors, status=status.HTTP_400_BAD_REQUEST)
