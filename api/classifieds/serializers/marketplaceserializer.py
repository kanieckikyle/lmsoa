from rest_framework import serializers

from classifieds.models import MarketplaceListing, ListingAttachment
from accounts.serializers import OwnerContactSerializer
from classifieds.fields import FileStringField


class MarketplaceListingSerializer(serializers.ModelSerializer):

    author = OwnerContactSerializer(read_only=True)
    attachments = serializers.StringRelatedField(many=True, read_only=True)

    class Meta:
        model = MarketplaceListing
        fields = (
            'id',
            'type',
            'author',
            'title',
            'description',
            'price',
            'public',
            'attachments',
            'timestamp',
        )

        read_only_fields = ('id', 'type', 'author', 'attachments')

    def create(self, validated_data, request):
        validated_data['author'] = request.user
        listing = super().create(validated_data)
        return listing
