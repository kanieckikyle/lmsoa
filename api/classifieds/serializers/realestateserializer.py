from rest_framework import serializers

from classifieds.models import RealEstateListing
from accounts.serializers import OwnerContactSerializer


class RealEstateListingSerializer(serializers.ModelSerializer):

    author = OwnerContactSerializer(read_only=True)
    attachments = serializers.StringRelatedField(many=True, read_only=True)

    class Meta:
        model = RealEstateListing
        fields = (
            'id',
            'lot_number',
            'title',
            'description',
            'type',
            'author',
            'price',
            'public',
            'attachments',
            'timestamp'
        )

        read_only_fields = ('id', 'type', 'author', 'attachments')

    def create(self, validated_data, request):
        validated_data['author'] = request.user
        listing = super().create(validated_data)
        return listing
