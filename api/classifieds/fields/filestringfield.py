from rest_framework import serializers
from django.conf import settings
from django.core.files import File
import io

class FileStringField(serializers.Field):
    def to_representation(self, obj):
        attachments = []
        for attachment in obj.all():
            attachments.append(settings.MEDIA_URL + attachment.file.name)

        return attachments

    def to_internal_value(self, data):
        return data
