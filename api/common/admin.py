from django.contrib import admin

from common.models import FirewiseEntry
# Register your models here.

class FirewiseEntryConsole(admin.ModelAdmin):
    list_display = ('id', 'level', 'date')

admin.site.register(FirewiseEntry, FirewiseEntryConsole)
