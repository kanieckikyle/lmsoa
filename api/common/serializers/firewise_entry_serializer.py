from rest_framework import serializers

from common.models import FirewiseEntry


class FirewiseEntrySerializer(serializers.ModelSerializer):

    class Meta:
        model = FirewiseEntry
        fields = (
            'id',
            'level',
            'date'
        )
