from .firewise_entry_serializer import FirewiseEntrySerializer

__all__ = [
    'FirewiseEntrySerializer'
]
