from django.db import models


class FirewiseEntry(models.Model):

    FIREWISE_CHOICES = (
        (1, 'Low'),
        (2, 'Moderate'),
        (3, 'High'),
        (4, 'Very High'),
        (5, 'Extreme')
    )

    level = models.PositiveIntegerField(choices=FIREWISE_CHOICES, null=False, blank=False)

    date = models.DateField(auto_now=True, editable=False)
