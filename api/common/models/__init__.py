from .firewise import FirewiseEntry

__all__ = [
    'FirewiseEntry'
]
