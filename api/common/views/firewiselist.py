from rest_framework import permissions, status
from rest_framework.views import APIView, Response

from common.serializers import FirewiseEntrySerializer
from common.models import FirewiseEntry


class FirewiseList(APIView):

    def get(self, request, format=None):
        entries = FirewiseEntry.objects.all().order_by('-date', '-id')
        serializer = FirewiseEntrySerializer(entries, many=True)
        return Response(data=serializer.data, status=status.HTTP_200_OK)

    def post(self, request, format=None):
        serializer = FirewiseEntrySerializer(data=request.data)

        if serializer.is_valid():
            entry = serializer.create(serializer.validated_data)
            return Response(data=FirewiseEntrySerializer(entry).data, status=status.HTTP_200_OK)

        return Response(data=serializer.errors, status=status.HTTP_400_BAD_REQUEST)
