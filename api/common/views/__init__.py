from .firewiselist import FirewiseList
from .firewise_detail import FirewiseEntryDetail

__all__ = [
    'FirewiseList',
    'FirewiseEntryDetail'
]
