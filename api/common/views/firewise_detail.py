from rest_framework import permissions, status
from rest_framework.views import APIView, Response
from django.http import Http404

from common.serializers import FirewiseEntrySerializer
from common.models import FirewiseEntry


class FirewiseEntryDetail(APIView):

    def get(self, request, pk, format=None):
        entry = self.get_model(pk)
        return Response(FirewiseEntrySerializer(entry).data, status=status.HTTP_200_OK)

    def delete(self, request, pk, format=None):
        entry = self.get_model(pk)
        entry.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

    def get_model(self, pk):
        try:
            return FirewiseEntry.objects.get(id=pk)
        except FirewiseEntry.DoesNotExist:
            raise Http404('Firewise entry with that id does not exist')
