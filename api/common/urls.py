from django.urls import path
from common.views import FirewiseEntryDetail, FirewiseList

urlpatterns = [
    path('firewise/', FirewiseList.as_view(), name='FirewiseList'),
    path('firewise/<int:pk>/', FirewiseEntryDetail.as_view(), name='FirewiseDetail')
]
