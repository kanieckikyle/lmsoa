from .serializers import JwtSerializer

def jwt_payload_handler(player):
    return JwtSerializer(instance=player).data
