from rest_framework import status, permissions
from rest_framework.views import Response, APIView

from accounts.serializers import RegisterSerializer, OwnerSerializer

class RegisterView(APIView):

    permission_classes = (permissions.AllowAny, )

    def post(self, request, format=None):

        serializer = RegisterSerializer(data=request.data)

        if serializer.is_valid():
            owner = serializer.create(serializer.validated_data)

            return Response(data=OwnerSerializer(instance=owner).data, status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
