from rest_framework.views import APIView, Response
from rest_framework import status
from django.db.models import Q

from accounts.models import Owner
from accounts.serializers import OwnerSerializer

class OwnerList(APIView):

    def get(self, request, format=None):

        page = int(request.GET.get('page', 1))
        page_length = int(request.GET.get('page_length', 25))
        start = page * page_length - 25
        end = page * page_length

        owners = self.filterQueryset(request, Owner.objects.all())

        data = {
            'page': page,
            'page_length': page_length,
            'count': len(owners),
            'items': OwnerSerializer(owners[start:end], many=True).data
        }

        return Response(data=data, status=status.HTTP_200_OK)

    def filterQueryset(self, request, queryset):
        query = request.GET.get('query', '')

        q = Q(hidden=False) & \
            Q(first_name__icontains=query) | Q(last_name__icontains=query) \
            | Q(address__icontains=query) | Q(lot_number__icontains=query) \
            | Q(cell_phone__icontains=query) | Q(home_phone__icontains=query)

        return queryset.filter(q)
