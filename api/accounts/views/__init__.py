from .ownerlist import OwnerList
from .register import RegisterView
from .ownerdetail import OwnerDetail
from .csv_upload import CsvUpload

__all__ = [
    'OwnerList',
    'RegisterView',
    'OwnerDetail',
    'CsvUpload'
]
