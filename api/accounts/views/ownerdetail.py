from rest_framework.views import APIView, Response
from rest_framework import status

from accounts.models import Owner
from accounts.serializers import OwnerSerializer, OwnerFullSerializer

class OwnerDetail(APIView):

    def get(self, request, pk, format=None):
        try:
            owner = Owner.objects.get(id=pk)
        except Owner.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        if request.user == owner:
            serializer = OwnerFullSerializer(instance=owner)
        else:
            serializer = OwnerSerializer(instance=owner)

        return Response(data=serializer.data, status=status.HTTP_200_OK)

    def patch(self, request, pk, format=None):
        try:
            owner = Owner.objects.get(id=pk)
        except Owner.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

        serializer = OwnerFullSerializer(instance=owner, data=request.data, partial=True)

        if serializer.is_valid():
            owner = serializer.update(owner, serializer.validated_data)
            return Response(data=OwnerFullSerializer(instance=owner).data, status=status.HTTP_200_OK)

        return Response(data=serializer.errors, status=status.HTTP_400_BAD_REQUEST)
