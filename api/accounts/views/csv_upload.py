from rest_framework.views import APIView, Response
from rest_framework import status
from django.db.models import Q
import logging
from csv import DictReader
import re
import io
import uuid

from accounts.models import Owner
from accounts.serializers import OwnerSerializer

logger = logging.getLogger(__name__)


class CsvUpload(APIView):

    def post(self, request, format=None):
        csv = request.data.get('csv').read().decode('utf-8')
        csv = io.StringIO(csv)
        people, errors = self.parse_csv(csv)
        people = list(map(lambda person: Owner(**person), people))
        new_owners = Owner.objects.bulk_create(people)
        serializer = OwnerSerializer(new_owners, many=True)

        return Response(data={
            'owners': serializer.data,
            'errors': errors
        }, status=status.HTTP_200_OK)

    def parse_csv(self, csv):
        reader = DictReader(csv)
        index = 0
        people = []
        name_regex = re.compile('([a-zA-z]+)\s*')
        unit_regex = re.compile('([0-9]+)\ (.+)')
        parse_errors = []
        for row in reader:
            if index == 0:
                index += 1
                continue

            name = name_regex.findall(row.get('Name', ''))
            if len(name) < 2:
                parse_errors.append(row)
                continue

            unit = unit_regex.findall(row.get("Unit names", ''))
            if len(unit) == 0:
                parse_errors.append(row)
                continue

            group = unit[0]
            if len(group) < 2:
                parse_errors.append(row)
                continue

            people.append({
                'username': uuid.uuid4().hex[:10].upper(),
                'first_name': name[0].capitalize(),
                'last_name': name[1].capitalize(),
                'email': row.get('email', ''),
                'home_phone': re.sub('\D', '', row.get('Contact no', ''))[:10],
                'lot_number': group[0],
                'address': group[1]
            })
            index += 1
        return people, parse_errors
