from django.core.management.base import BaseCommand
import random

from accounts.models import Owner


class Command(BaseCommand):

    def handle(self, *args, **options):
        accs = []
        for i in range(100):
            accs.append(
                Owner(
                    username='test{}'.format(i),
                    password='123qweasd',
                    email='test@test.com',
                    first_name='test{}'.format(i),
                    last_name='test{}'.format(i),
                    lot_number=str(i),
                    cell_phone='1234567890',
                    home_phone='1234567890',
                    address='123 hello st'
                )
            )

        Owner.objects.bulk_create(accs)
