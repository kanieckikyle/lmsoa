from django.core.management.base import BaseCommand
import random

from lmsoamessages.models import Message
from accounts.models import Owner


class Command(BaseCommand):

    def handle(self, *args, **options):

        messages = []
        owners = list(Owner.objects.all())
        for i in range(100):
            messages.append(
                Message(
                    title='test{}'.format(i),
                    body='Testing this shit man test{}'.format(i),
                    author=random.choice(owners)
                )
            )

        Message.objects.bulk_create(messages)
