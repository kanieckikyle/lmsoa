from django.core.management.base import BaseCommand
import random

from lmsoamessages.models import Message
from accounts.models import Owner


class Command(BaseCommand):

    def handle(self, *args, **options):
        if not Owner.objects.filter(username='tabledwler').exists():
            owner = Owner.objects.create(
                username="tabledwler",
                email="kanieckikyle@gmail.com",
                first_name="Kyle",
                last_name="Kaniecki",
                cell_phone='4404794617',
                home_phone='4404794617',
                lot_number=303,
                is_staff=True,
                is_superuser=True,
                hidden=True
            )

            owner.set_password('supersecurepassword123!@#')
            owner.save()

        if not Owner.objects.filter(username='kkaniecki').exists():
            owner = Owner.objects.create(
                username="kkaniecki",
                email="kenneth.kaniecki@gmail.com",
                first_name="Ken",
                last_name="Kaniecki",
                cell_phone='2169048708',
                home_phone='2169048708',
                lot_number=303,
                is_staff=True,
                is_superuser=True,
                hidden=False
            )

            owner.set_password('supersecurepassword123!@#')
            owner.save()
