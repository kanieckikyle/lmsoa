from django.urls import path, include
from rest_framework_jwt.views import obtain_jwt_token, refresh_jwt_token

from .views import OwnerList, RegisterView, OwnerDetail, CsvUpload

urlpatterns = [
    path('owners/', OwnerList.as_view(), name='OwnerList'),
    path('owners/<int:pk>/', OwnerDetail.as_view(), name='OwnerDetail'),
    path('register/', RegisterView.as_view(), name='RegisterView'),
    path('auth/token/', obtain_jwt_token, name='GetJwtToken'),
    path('auth/token/refresh/', refresh_jwt_token, name='RefreshJwtToken'),
    path('owners/upload/bulk/', CsvUpload.as_view(), name="CsvUpload")
]
