from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from accounts.models import Owner
from django.utils.translation import ugettext_lazy as _

# Register your models here.

class OwnerAdminConsole(UserAdmin):
    list_display = ('id', 'username', 'email', 'first_name', 'last_name')
    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        (_('Personal info'), {'fields': ('first_name', 'last_name', 'email')}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',
                                       'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
        (_('Contact Info'), {'fields': ('lot_number', 'cell_phone', 'home_phone', 'address')}),
        (_('Other'), {'fields': ('avatar', 'hidden')})
    )


admin.site.register(Owner, OwnerAdminConsole)
