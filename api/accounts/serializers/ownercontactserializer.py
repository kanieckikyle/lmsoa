from accounts.models import Owner
from rest_framework import serializers

class OwnerContactSerializer(serializers.ModelSerializer):

    class Meta:
        model = Owner
        fields = (
            'id',
            'username',
            'email',
            'first_name',
            'last_name',
            'cell_phone',
            'home_phone',
            'avatar'
        )
