from accounts.models import Owner
from rest_framework import serializers

class OwnerSerializer(serializers.ModelSerializer):

    cell_phone = serializers.SerializerMethodField()
    home_phone = serializers.SerializerMethodField()
    address = serializers.SerializerMethodField()

    class Meta:
        model = Owner
        fields = (
            'id',
            'username',
            'email',
            'first_name',
            'last_name',
            'cell_phone',
            'home_phone',
            'address',
            'lot_number',
            'avatar',
            'is_staff',
            'is_superuser'
        )

    def get_cell_phone(self, owner):
        if owner.hidden:
            return 'hidden'
        return owner.cell_phone

    def get_home_phone(self, owner):
        if owner.hidden:
            return 'hidden'
        return owner.home_phone

    def get_address(self, owner):
        if owner.hidden:
            return 'hidden'
        return owner.address
