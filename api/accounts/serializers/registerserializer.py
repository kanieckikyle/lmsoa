from rest_framework import serializers
from accounts.models import Owner

class RegisterSerializer(serializers.ModelSerializer):

    avatar = serializers.ImageField(max_length=255, allow_empty_file=True, required=False)

    class Meta:
        model = Owner
        fields = (
            'username',
            'email',
            'password',
            'first_name',
            'last_name',
            'lot_number',
            'cell_phone',
            'home_phone',
            'address',
            'avatar'
        )

        required_fields = (
            'username',
            'email',
            'password',
            'first_name',
            'last_name'
        )

    def create(self, validated_data):
        owner = super().create(validated_data=validated_data)
        owner.set_password(validated_data.get('password'))
        owner.save()
        return owner
