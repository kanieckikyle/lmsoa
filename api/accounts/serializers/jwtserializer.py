from accounts.models import Owner
from rest_framework import serializers
from datetime import datetime
from calendar import timegm
from rest_framework_jwt.settings import api_settings


class JwtSerializer(serializers.ModelSerializer):

    exp = serializers.SerializerMethodField()
    orig_iat = serializers.SerializerMethodField()
    permissions = serializers.SerializerMethodField()

    class Meta:
        model = Owner
        fields = (
            'id',
            'username',
            'email',
            'first_name',
            'last_name',
            'last_login',
            'date_joined',
            'exp',
            'orig_iat',
            'is_active',
            'is_staff',
            'is_superuser',
            'permissions'
        )

    def get_exp(self, obj):
        return datetime.utcnow() + api_settings.JWT_EXPIRATION_DELTA

    def get_orig_iat(self, obj):
        return timegm(datetime.utcnow().utctimetuple())

    def get_permissions(self, obj):
        return [str(permission)
                for permission in obj.user_permissions.all().values_list('codename', flat=True)]
