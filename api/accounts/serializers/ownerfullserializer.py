from accounts.models import Owner
from rest_framework import serializers

class OwnerFullSerializer(serializers.ModelSerializer):

    class Meta:
        model = Owner
        fields = (
            'id',
            'username',
            'email',
            'first_name',
            'last_name',
            'cell_phone',
            'home_phone',
            'address',
            'lot_number',
            'avatar',
            'is_staff',
            'is_superuser'
        )

        read_only_fields = ('id', 'is_superuser', 'is_staff',)
