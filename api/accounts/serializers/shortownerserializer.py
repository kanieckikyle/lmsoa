from accounts.models import Owner
from rest_framework import serializers

class ShortOwnerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Owner
        fields = (
            'id',
            'first_name',
            'last_name',
            'avatar',
            'is_superuser'
        )
