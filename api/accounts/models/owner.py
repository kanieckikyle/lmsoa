from django.db import models
from django.contrib.auth.models import AbstractUser, UserManager
from django.utils.translation import ugettext_lazy as _


class OwnerManager(UserManager):
    def get_by_natural_key(self, username):
        case_insensitive_username_field = '{}__iexact'.format(self.model.USERNAME_FIELD)
        return self.get(**{case_insensitive_username_field: username})


class Owner(AbstractUser):
    objects = OwnerManager()

    cell_phone = models.CharField(max_length=10, null=True, blank=True)

    home_phone = models.CharField(max_length=10, null=True, blank=True)

    lot_number = models.CharField(max_length=5, null=True, blank=True)

    address = models.CharField(max_length=250, null=True, blank=True)

    avatar = models.ImageField(null=True, blank=True)

    hidden = models.BooleanField(default=False)

    class Meta:
        verbose_name = _('owner')
        verbose_name_plural = _('owners')
