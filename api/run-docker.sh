#!/usr/bin/env bash

# Run everything in the background with -d
docker-compose up --build -d

# Tail out all of the logs
docker-compose logs --tail=0 --follow

