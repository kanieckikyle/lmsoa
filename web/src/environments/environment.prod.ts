export const environment = {
  production: true,
  base_url: 'http://prod-lmsoa.us-east-1.elasticbeanstalk.com/api/v1'
};
