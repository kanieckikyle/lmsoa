import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoggedInGuard } from './accounts/guards/logged-in.guard';

const routes: Routes = [
  {
    path: 'messages',
    loadChildren: './message-board/message-board.module#MessageBoardModule',
    canActivate: [LoggedInGuard]
  },
  {
    path: 'classifieds',
    loadChildren: './classifieds/classifieds.module#ClassifiedsModule'
  },
  {
    path: 'calendar',
    loadChildren: './calendar/calendar.module#CalendarModule'
  },
  {
    path: 'dues',
    loadChildren: './dues/dues.module#DuesModule'
  },
  {
    path: 'dashboard',
    loadChildren: './dashboard/dashboard.module#DashboardModule'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
