import { GenericDataSource } from '../../base/dataSources/generic-datasource';
import { OwnerService } from '../services/owner.service';

export class AddressBookDataSource extends GenericDataSource<any> {

    _query: string;

    constructor(
        private ownerService: OwnerService,
        {
        query = null,
        page = 0,
        pageLength = 25
        } = {}
    ) {
        super({page: page, pageLength: pageLength});
        this._query = query;
        this.update();
    }

    update() {
        this._loading.next(true);
        this.ownerService.getOwners(this._filterPredicate())
        .subscribe((owners: any) => {
            this.count = owners.count;
            this.page_length = owners.page_length;
            this.updatePaginator();
            this._renderData.next(owners.items);
            this._loading.next(false);
        });
    }

    set query(query: string | null) {
        this._query = query;
        this.updatePaginator();
    }
}
