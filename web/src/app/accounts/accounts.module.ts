import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import {NgxMaskModule} from 'ngx-mask';
import {MatStepperModule} from '@angular/material/stepper';

import { AccountsRoutingModule } from './accounts-routing.module';
import { LoginComponent } from './pages/login/login.component';
import { BaseModule } from '../base/base.module';
import { AddressBookComponent } from './pages/address-book/address-book.component';
import { RegisterComponent } from './pages/register/register.component';
import { ProfilePageComponent } from './pages/profile-page/profile-page.component';


@NgModule({
  declarations: [LoginComponent, AddressBookComponent, RegisterComponent, ProfilePageComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    AccountsRoutingModule,
    BaseModule,
    NgxMaskModule.forRoot(),
    MatStepperModule
  ]
})
export class AccountsModule { }
