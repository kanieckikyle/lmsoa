import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../services/authentication.service';
import { FormGroup, FormControl, Validators, ValidationErrors, ValidatorFn } from '@angular/forms';
import { switchMap } from 'rxjs/operators';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  form: FormGroup = new FormGroup({
    username: new FormControl('', [
      Validators.required
    ]),
    password: new FormControl('', [
      Validators.required
    ]),
    confirm_password: new FormControl('', [
      Validators.required
    ]),
    email: new FormControl('', [
      Validators.required,
      Validators.email
    ]),
    first_name: new FormControl('', [
      Validators.required
    ]),
    last_name: new FormControl('', [
      Validators.required
    ]),
    lot_number: new FormControl('', [
      Validators.min(0),
      Validators.max(1000),
      Validators.required
    ]),
    cell_phone: new FormControl('', [
      Validators.minLength(10),
    ]),
    home_phone: new FormControl('', [
      Validators.minLength(10)
    ]),
    address: new FormControl(''),
    accept_tos: new FormControl(false),
    hidden: new FormControl(false)
  }, [
    this.samePasswordValidator
  ]);

  constructor(private authService: AuthenticationService, private router: Router) { }

  ngOnInit() {
  }

  register() {
    this.authService.register(this.form.getRawValue())
    .pipe(
      switchMap(_ => this.authService.login({username: this.username.value, password: this.password.value}))
    )
    .subscribe((result: any) => this.router.navigate(['/']));
  }

  private samePasswordValidator(formGroup: FormGroup): ValidationErrors | null {
    if (formGroup.get('password').value !== formGroup.get('confirm_password').value) {
      formGroup.get('confirm_password').setErrors({samePassword: true});
      return {samePassword: true};
    };
    formGroup.get('confirm_password').setErrors(null);
    return null;
  }

  get username() {
    return this.form.get('username');
  }

  get password() {
    return this.form.get('password');
  }

  get confirm_password() {
    return this.form.get('confirm_password');
  }

  get email() {
    return this.form.get('email');
  }

  get first_name() {
    return this.form.get('first_name');
  }

  get last_name() {
    return this.form.get('last_name');
  }

  get lot_number() {
    return this.form.get('lot_number');
  }

  get cell_phone() {
    return this.form.get('cell_phone');
  }

  get home_phone() {
    return this.form.get('home_phone');
  }

  get address() {
    return this.form.get('address');
  }

  get accept_tos() {
    return this.form.get('accept_tos');
  }

  get hidden() {
    return this.form.get('hidden');
  }

}
