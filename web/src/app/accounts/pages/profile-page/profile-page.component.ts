import { Component, OnInit, ViewChild } from '@angular/core';
import { OwnerService } from '../../services/owner.service';
import { AuthenticationService } from '../../services/authentication.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { trigger, state, style, transition, animate } from '@angular/animations';

@Component({
  selector: 'app-profile-page',
  templateUrl: './profile-page.component.html',
  styleUrls: ['./profile-page.component.scss'],
  animations: [
    trigger('fade', [
      state('void', style({ opacity: 0 })),
      state('*', style({ opacity: 1 })),
      transition(':enter', animate('200ms ease-out')),
      transition(':leave', animate('200ms ease-in')),
    ])
  ]
})
export class ProfilePageComponent implements OnInit {

  owner: any;
  @ViewChild('avatarInput') avatarInput;
  profilePicHover = false;
  profileUrl: string;

  form: FormGroup = new FormGroup({
    username: new FormControl('', [
      Validators.required
    ]),
    password: new FormControl('', [
      Validators.required
    ]),
    confirm_password: new FormControl('', [
      Validators.required
    ]),
    email: new FormControl('', [
      Validators.required,
      Validators.email
    ]),
    first_name: new FormControl('', [
      Validators.required
    ]),
    last_name: new FormControl('', [
      Validators.required
    ]),
    lot_number: new FormControl('', [
      Validators.min(0),
      Validators.max(1000),
      Validators.required
    ]),
    cell_phone: new FormControl('', [
      Validators.minLength(10),
    ]),
    home_phone: new FormControl('', [
      Validators.minLength(10)
    ]),
    address: new FormControl(''),
    accept_tos: new FormControl(false),
    hidden: new FormControl(false)
  });
  showSuccess = false;

  constructor(private ownerService: OwnerService, private authService: AuthenticationService) { }

  ngOnInit() {
    this.ownerService.getOwner(this.authService.currentUser.id)
      .subscribe((owner: any) => {
        this.owner = owner;
        this.form.patchValue(this.owner);
        this.form.patchValue(this.authService.currentUser);
      });
  }

  updateProfile() {
    if (this.form.errors) {
      return;
    }

    this.ownerService.updateOwner(this.owner.id, this.getFormData())
      .subscribe((result: any) => {
        this.owner = result;
        this.form.patchValue(result);
        this.showSuccess = true;
        setTimeout(() => this.showSuccess = false, 3000);
      });
  }

  getAvatarUrl() {

  }

  private getFormData() {
    const formData = new FormData();
    const formValue = this.form.getRawValue();
    Object.keys(formValue).forEach(key => formData.append(key, formValue[key]));
    if (this.avatarInput.nativeElement.files.length > 0) {
      formData.append('avatar', this.avatarInput.nativeElement.files[0]);
    }
    return formData;
  }

  get username() {
    return this.form.get('username');
  }

  get email() {
    return this.form.get('email');
  }

  get first_name() {
    return this.form.get('first_name');
  }

  get last_name() {
    return this.form.get('last_name');
  }

  get lot_number() {
    return this.form.get('lot_number');
  }

  get cell_phone() {
    return this.form.get('cell_phone');
  }

  get home_phone() {
    return this.form.get('home_phone');
  }

  get address() {
    return this.form.get('address');
  }

  get hidden() {
    return this.form.get('hidden');
  }

}
