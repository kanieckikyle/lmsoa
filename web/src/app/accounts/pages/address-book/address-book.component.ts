import { Component, OnInit, ViewChild } from '@angular/core';
import { OwnerService } from '../../services/owner.service';
import { MatPaginator } from '@angular/material/paginator';
import { AddressBookDataSource } from '../../dataSources/addressbook-datasource';
import { FormControl } from '@angular/forms';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
import { GlobalService } from 'src/app/base/services/global.service';

@Component({
  selector: 'app-address-book',
  templateUrl: './address-book.component.html',
  styleUrls: ['./address-book.component.scss']
})
export class AddressBookComponent implements OnInit {

  displayedColumns = ['name', 'lot', 'cell', 'home', 'address'];
  addressBookDataSource: AddressBookDataSource;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  filterControl: FormControl = new FormControl('');

  constructor(private ownerService: OwnerService, private globalService: GlobalService) { }

  ngOnInit() {
    this.addressBookDataSource = new AddressBookDataSource(this.ownerService);
    this.addressBookDataSource.paginator = this.paginator;

    this.filterControl.valueChanges
    .pipe(
      debounceTime(400),
      distinctUntilChanged(),
      map((value: string) => value !== '' ? value : null),
    )
    .subscribe((value: string | null) => {
      this.addressBookDataSource.query = value;
      this.addressBookDataSource.update();
    });

    this.globalService.screenSize$
    .subscribe((size: string) => {
      if (size === 'x-small') {
        this.displayedColumns = this.displayedColumns.filter(column => column !== 'address');
      } else if (!this.displayedColumns.find(column => column === 'address')){
        this.displayedColumns.push('address');
      }
    })
  }

}
