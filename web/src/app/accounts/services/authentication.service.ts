import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { tap } from 'rxjs/operators';
import { BehaviorSubject, Observable, interval } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private jwtHelper = new JwtHelperService();
  private isLoggedIn: BehaviorSubject<boolean> = new BehaviorSubject(localStorage.getItem('token') !== null);

  constructor(private http: HttpClient) {
    if (this.getToken() !== null) {
      this.refreshToken(true);
      interval(1000 * 60 * 5)
      .subscribe(_ => this.refreshToken(true))
    }
  }

  login(data: any) {
    return this.http
    .post(
      `${environment.base_url}/auth/token/`,
      data
    )
    .pipe(
      tap((result: any) => {
        localStorage.setItem('token', result.token);
        this.isLoggedIn.next(true);
      })
    );
  }

  refreshToken(subscribe: boolean = false): Observable<any> | null {
    const call = this.http
    .post(
      `${environment.base_url}/auth/token/refresh/`,
      {token: this.getToken()}
    )
    .pipe(
      tap((result: any) => localStorage.setItem('token', result['token']))
    );

    if (subscribe) {
      call.subscribe();
      return null;
    }

    return call;
  }

  logout() {
    this.isLoggedIn.next(false);
    localStorage.removeItem('token');
  }

  register(data: FormData): Observable<any> {
    return this.http
    .post(
      `${environment.base_url}/register/`,
      data
    );
  }

  private getToken(): string | null {
    return localStorage.getItem('token');
  }

  get currentUser(): any | null {
    if (this.isLoggedIn.value) {
      return this.jwtHelper.decodeToken(this.getToken());
    }
    return null;
  }

  get isLoggedIn$(): Observable<boolean> {
    return this.isLoggedIn.asObservable();
  }
}
