import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class OwnerService {

  constructor(private http: HttpClient) {}

  getOwners(params?: string) {
    const url = `${environment.base_url}/owners/${params}`;
    return this.http
    .get(
      url,
      this.getJsonHeaders()
    );
  }

  getOwner(id: number) {
    return this.http
    .get(`${environment.base_url}/owners/${id}/`, this.getJsonHeaders());
  }

  updateOwner(id: number, data: FormData) {
    return this.http
    .patch(
      `${environment.base_url}/owners/${id}/`,
      data
    );
  }

  private getJsonHeaders(): any {
    return { headers: new HttpHeaders().set('Content-Type', 'application/json')};
  }
}
