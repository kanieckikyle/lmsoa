import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './pages/login/login.component';
import { AddressBookComponent } from './pages/address-book/address-book.component';
import { RegisterComponent } from './pages/register/register.component';
import { LoggedInGuard } from './guards/logged-in.guard';
import { ProfilePageComponent } from './pages/profile-page/profile-page.component';

const routes: Routes = [
    {
        path: 'login',
        component: LoginComponent
    },
    {
        path: 'addressbook',
        component: AddressBookComponent,
        canActivate: [LoggedInGuard]
    },
    {
      path: 'register',
      component: RegisterComponent
    },
    {
      path: 'profile',
      component: ProfilePageComponent
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccountsRoutingModule { }
