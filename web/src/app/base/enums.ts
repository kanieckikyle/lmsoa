export enum FirewiseLevel {
  Low = 1,
  Moderate = 2,
  High = 3,
  VHigh = 4,
  Extreme = 5
}
