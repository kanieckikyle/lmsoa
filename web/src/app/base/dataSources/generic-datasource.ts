import {distinctUntilChanged, debounceTime} from 'rxjs/operators';
import {DataSource} from '@angular/cdk/collections';
import {BehaviorSubject} from 'rxjs';
import {MatPaginator} from '@angular/material/paginator';


export class GenericDataSource<T> extends DataSource<T> {

  protected readonly _renderData = new BehaviorSubject<T[]>([]);
  protected readonly _loading = new BehaviorSubject<boolean>(true);

  isLoading$ = this._loading.asObservable();

  protected count = 0;
  protected page_length = 0;

  _page = 0;
  _page_length = 10;

  _paginator: MatPaginator;

  constructor({page = 0, pageLength = 10} = {}) {
    super();
    this._page = page;
    this._page_length = pageLength;
  }

  connect() {
    return this._renderData;
  }

  disconnect() {
    return;
  }

  update() {
  }

  updatePaginator() {
    if (this._paginator) {
      this._paginator.length = this.count;
      this._paginator.pageSize = this.page_length;
    }
  }

  /**
   * Gets the query string for the API call using class reflection. I found this to be very elegant,
   * but not sure if I should ever user reflection
   */
  _filterPredicate() {
    let queryParams = '?';
    const queries = Object.getOwnPropertyNames(this)
    .filter(param => param.startsWith('_') && param !== '_data' && param !== '_renderData' && param !== '_paginator' && param !== '_loading');
    for (const query of queries) {
      if (this[query] !== null) {
        if (queryParams.length > 1) {
          queryParams += '&';
        }
        if (query === '_page') {
          queryParams += `${query.substring(1)}=${this[query] + 1}`;
        } else {
          queryParams += `${query.substring(1)}=${this[query]}`;
        }
      }
    }
    return queryParams;
  }

  set page(page: number) {
    this._page = page;
    this.update();
  }

  set pageLength(pageLength: number) {
    this._page_length = pageLength;
    this.page_length = pageLength;
    this.update();
  }

  set paginator(paginator: MatPaginator) {
    this._paginator = paginator;
    this._paginator.pageSize = this.page_length;
    this._paginator.page
      .pipe(
        debounceTime(400),
        distinctUntilChanged()
      )
      .subscribe(page => {
        if (!this._loading.getValue()) {
          this._page = page.pageIndex;
          this.update();
        }
      });
  }

  getCount() {
    return this.count;
  }

  get data() {
    return this._renderData.value;
  }
}
