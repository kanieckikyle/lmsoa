import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { TermsPageComponent } from './pages/terms-page/terms-page.component';
import { FirewisePageComponent } from './pages/firewise-page/firewise-page.component';

const routes: Routes = [
    {
        path: '',
        component: HomeComponent
    },
    {
      path: 'tos',
      component: TermsPageComponent
    },
    {
      path: 'firewise',
      component: FirewisePageComponent
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BaseRoutingModule { }
