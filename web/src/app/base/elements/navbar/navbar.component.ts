import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/accounts/services/authentication.service';
import { Router } from '@angular/router';
import { GlobalService } from '../../services/global.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  isLoggedIn = false;
  isMobile = true;
  isAdmin = false;

  constructor(private authService: AuthenticationService, private router: Router, private globalService: GlobalService) {}

  ngOnInit() {
    this.authService.isLoggedIn$
    .pipe(
      map((loggedIn: boolean) => {
        this.isLoggedIn = loggedIn;
        if (loggedIn) {
          return this.authService.currentUser
        }
        return null;
      })
    )
    .subscribe((user: any) => {
      if (user !== null) {
        this.isAdmin = user.is_superuser;
      }
    });

    this.globalService.screenSize$
    .subscribe((size: string) => {
      switch(size) {
        case 'small':
        case 'x-small':
          this.isMobile = true;
          break;
        default:
          this.isMobile = false;
      }
    });
  }

  logout() {
    this.authService.logout();
    this.router.navigate(['/']);
  }

  get username() {
    return this.authService.currentUser.first_name;
  }

  get screenSize$(): Observable<string> {
    return this.globalService.screenSize$;
  }

}
