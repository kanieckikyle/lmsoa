import { Component, OnInit, Input } from '@angular/core';
import { AuthenticationService } from 'src/app/accounts/services/authentication.service';

@Component({
  selector: 'app-chat-bubble',
  templateUrl: './chat-bubble.component.html',
  styleUrls: ['./chat-bubble.component.scss']
})
export class ChatBubbleComponent implements OnInit {

  @Input() right = false;
  @Input() message: string;
  @Input() headerText: string;
  @Input() footerText: string;
  @Input() backgroundColor: string;
  @Input() fixedWidth: string;
  @Input() comment: any;

  constructor(private authService: AuthenticationService) { }

  ngOnInit() {
    this.authService.isLoggedIn$
    .subscribe((loggedIn: boolean) => {
      // this.right = loggedIn && this.authService.currentUser.id === this.comment.author.id;
      if (this.authService.currentUser.id === this.comment.author.id) {
        this.backgroundColor = 'lightblue';
      }
    });
  }

  get isAdmin() {
    return this.comment.author.is_superuser;
  }

}
