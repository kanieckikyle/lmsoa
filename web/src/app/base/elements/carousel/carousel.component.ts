import { Component, OnInit, AfterViewInit, ContentChildren, QueryList, Directive, ViewChildren, ElementRef, ViewChild, Input } from '@angular/core';
import { CarouselItemDirective } from '../../directives/carousel-item.directive';
import { AnimationPlayer, animate, style, AnimationFactory, AnimationBuilder } from '@angular/animations';
import { InternalCarouselItemDirective } from '../../directives/internal-carousel-item.directive';

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.scss']
})
export class CarouselComponent implements OnInit, AfterViewInit {

  @ViewChild('carousel') private carousel: ElementRef;
  @ContentChildren(CarouselItemDirective) items: QueryList<CarouselItemDirective>;
  @ViewChildren(InternalCarouselItemDirective, { read: ElementRef }) private itemsElements: QueryList<ElementRef>;
  carouselWrapperStyle: { width: string; };
  buttonStyle: { height: string };

  @Input() timing = '250ms ease-in';
  private player: AnimationPlayer;
  private itemWidth: number = 0;
  private itemHeight: number = 0;
  private currentSlide = 0;
  showControls = true;

  constructor(private builder: AnimationBuilder) { }

  ngOnInit() {
  }

  ngAfterViewInit() {

    setTimeout(() => {
      this.itemWidth = this.itemsElements.first.nativeElement.getBoundingClientRect().width;
      this.carouselWrapperStyle = {
        width: `${this.itemWidth}px`
      }
      this.itemHeight = this.itemsElements.first.nativeElement.getBoundingClientRect().height;
      this.buttonStyle = {
        height: `${this.itemHeight}px`
      }
    });
  }

  next() {
    if (this.currentSlide + 1 === this.items.length) return;

    this.currentSlide = (this.currentSlide + 1) % this.items.length;

    const offset = this.currentSlide * this.itemWidth;

    const myAnimation: AnimationFactory = this.builder.build([
      animate(this.timing, style({ transform: `translateX(-${offset}px)` }))
    ]);

    this.player = myAnimation.create(this.carousel.nativeElement);
    this.player.play();
  }
  prev() {
    if (this.currentSlide === 0) return;

    this.currentSlide = ((this.currentSlide - 1) + this.items.length) % this.items.length;
    const offset = this.currentSlide * this.itemWidth;

    const myAnimation: AnimationFactory = this.builder.build([
      animate(this.timing, style({ transform: `translateX(-${offset}px)` }))
    ]);

    this.player = myAnimation.create(this.carousel.nativeElement);
    this.player.play();
  }

}
