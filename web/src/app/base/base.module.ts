import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatMenuModule} from '@angular/material/menu';
import {MatIconModule} from '@angular/material/icon';
import {MatTableModule} from '@angular/material/table';
import {MatPaginatorModule} from '@angular/material/paginator';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faThumbtack, faQuestionCircle, faTimesCircle, faCircle } from '@fortawesome/free-solid-svg-icons';
import {ScrollDispatchModule} from '@angular/cdk/scrolling';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatListModule} from '@angular/material/list';
import {MatSelectModule} from '@angular/material/select';
import {MatDialogModule} from '@angular/material/dialog';
import {MatDividerModule} from '@angular/material/divider';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatMomentDateModule} from '@angular/material-moment-adapter';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {NgxMaterialTimepickerModule} from 'ngx-material-timepicker';

library.add(
  faThumbtack,
  faQuestionCircle,
  faTimesCircle,
  faCircle
);

import { BaseRoutingModule } from './base-routing.module';
import { HomeComponent } from './pages/home/home.component';
import { NavbarComponent } from './elements/navbar/navbar.component';
import { ChatBubbleComponent } from './elements/chat-bubble/chat-bubble.component';
import { CarouselComponent } from './elements/carousel/carousel.component';
import { CarouselItemDirective } from './directives/carousel-item.directive';
import { InternalCarouselItemDirective } from './directives/internal-carousel-item.directive';
import { DeleteDialogComponent } from './elements/delete-dialog/delete-dialog.component';
import { TermsPageComponent } from './pages/terms-page/terms-page.component';
import { DynamicDialogComponent } from './elements/dynamic-dialog/dynamic-dialog.component';
import { FirewisePageComponent } from './pages/firewise-page/firewise-page.component';
import { FirewiseLevelPipe } from './pipes/firewise-level.pipe';


@NgModule({
  declarations: [
      HomeComponent,
      NavbarComponent,
      ChatBubbleComponent,
      CarouselComponent,
      CarouselItemDirective,
      InternalCarouselItemDirective,
      DeleteDialogComponent,
      TermsPageComponent,
      DynamicDialogComponent,
      FirewisePageComponent,
      FirewiseLevelPipe
  ],
  imports: [
    CommonModule,
    BaseRoutingModule,
    MatToolbarModule,
    MatButtonModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatMenuModule,
    MatIconModule,
    MatTableModule,
    MatPaginatorModule,
    FontAwesomeModule,
    ScrollDispatchModule,
    MatProgressSpinnerModule,
    MatCheckboxModule,
    MatTooltipModule,
    MatListModule,
    MatSelectModule,
    MatDialogModule,
    MatDividerModule,
    MatDatepickerModule,
    MatMomentDateModule,
    MatButtonToggleModule,
    NgxMaterialTimepickerModule.forRoot()
  ],
  exports: [
    MatButtonModule,
    NavbarComponent,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatMenuModule,
    MatIconModule,
    MatTableModule,
    MatPaginatorModule,
    FontAwesomeModule,
    ScrollDispatchModule,
    MatProgressSpinnerModule,
    MatCheckboxModule,
    ChatBubbleComponent,
    MatTooltipModule,
    CarouselComponent,
    CarouselItemDirective,
    MatListModule,
    MatSelectModule,
    DeleteDialogComponent,
    MatDialogModule,
    MatDividerModule,
    MatDatepickerModule,
    MatMomentDateModule,
    MatButtonToggleModule,
    DynamicDialogComponent,
    NgxMaterialTimepickerModule
  ],
  entryComponents: [
    DeleteDialogComponent,
    DynamicDialogComponent
  ]
})
export class BaseModule { }
