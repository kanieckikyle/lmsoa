import { Injectable } from '@angular/core';
import { fromEvent, Observable } from 'rxjs';
import { startWith, debounceTime, distinctUntilChanged, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class GlobalService {

  screenSize$: Observable<string>;

  constructor() {
    this.screenSize$ = fromEvent(window, 'resize')
    .pipe(
      startWith({ target: { innerWidth: window.innerWidth }}),
      debounceTime(200),
      distinctUntilChanged(),
      map((event: Event) => {
        const windowWidth = (event.target as Window).innerWidth;
        if (windowWidth >= 1200) {
          return 'x-large';
        } else if (windowWidth > 992) {
          return 'large';
        } else if ( windowWidth > 768) {
          return 'medium';
        } else if (windowWidth > 576) {
          return 'small';
        }
        return 'x-small';
      })
    );
  }
}
