import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { FirewiseLevel } from '../enums';

@Injectable({
  providedIn: 'root'
})
export class FirewiseService {

  constructor(private http: HttpClient) { }

  getFirewiseEntryList() {
    return this.http
    .get(
      `${environment.base_url}/common/firewise/`,
      this.getJsonHeaders()
    );
  }

  getFirewiseEntry(id: number) {
    return this.http
    .get(
      `${environment.base_url}/common/firewise/${id}/`,
      this.getJsonHeaders()
    );
  }

  createFirewiseEntry(level: FirewiseLevel) {
    return this.http
    .post(
      `${environment.base_url}/common/firewise/`,
      { level: level },
      this.getJsonHeaders()
    );
  }

  deleteFirewiseEntry(id: number) {
    return this.http
    .delete(
      `${environment.base_url}/common/firewise/${id}/`,
      this.getJsonHeaders()
    );
  }

  private getJsonHeaders(): {headers: HttpHeaders} {
    return { headers: new HttpHeaders().set('Content-Type', 'application/json') }
  }


}
