import { TestBed } from '@angular/core/testing';

import { FirewiseService } from './firewise.service';

describe('FirewiseService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FirewiseService = TestBed.get(FirewiseService);
    expect(service).toBeTruthy();
  });
});
