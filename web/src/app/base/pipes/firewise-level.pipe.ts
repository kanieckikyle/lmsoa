import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'firewiseLevel'
})
export class FirewiseLevelPipe implements PipeTransform {

  transform(value: number): string {
    switch (value) {
      case 1:
        return 'Low';
      case 2:
        return 'Moderate';
      case 3:
        return 'High';
      case 4:
        return 'Very High';
      case 5:
        return 'Extreme';
      default:
        return 'Unknown';
    }
  }

}
