import { Component, OnInit } from '@angular/core';
import { FirewiseService } from '../../services/firewise.service';
import { FirewiseLevel } from '../../enums';
import { MatTableDataSource } from '@angular/material/table'

interface FirewiseEntry {
  id: number;
  level: FirewiseLevel;
  date: string;
};

@Component({
  selector: 'app-firewise-page',
  templateUrl: './firewise-page.component.html',
  styleUrls: ['./firewise-page.component.scss']
})
export class FirewisePageComponent implements OnInit {

  firewiseEntries: FirewiseEntry[] = [];
  currentEntry: FirewiseEntry;

  pastFirewiseEntries: MatTableDataSource<FirewiseEntry> = new MatTableDataSource();

  displayedColumns = ['level', 'date'];

  constructor(private firewiseService: FirewiseService) { }

  ngOnInit() {
    this.firewiseService.getFirewiseEntryList()
    .subscribe((entries: FirewiseEntry[]) => {
      this.firewiseEntries = entries;
      this.currentEntry = this.firewiseEntries.shift();
      this.pastFirewiseEntries.data = this.firewiseEntries;
    });
  }

  get currentColor(): string {
    switch (this.currentEntry.level) {
      case FirewiseLevel.Low:
        return 'green';
      case FirewiseLevel.Moderate:
        return 'blue';
      case FirewiseLevel.High:
        return 'yellow';
      case FirewiseLevel.VHigh:
        return 'orange';
      case FirewiseLevel.Extreme:
        return 'red';
      default:
        return 'rgba(0, 0, 0, 0);'
    }
  }

}
