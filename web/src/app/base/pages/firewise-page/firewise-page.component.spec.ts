import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FirewisePageComponent } from './firewise-page.component';

describe('FirewisePageComponent', () => {
  let component: FirewisePageComponent;
  let fixture: ComponentFixture<FirewisePageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FirewisePageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FirewisePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
