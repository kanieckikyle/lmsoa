import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventEditCreateDialogComponent } from './event-edit-create-dialog.component';

describe('EventEditCreateDialogComponent', () => {
  let component: EventEditCreateDialogComponent;
  let fixture: ComponentFixture<EventEditCreateDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventEditCreateDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventEditCreateDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
