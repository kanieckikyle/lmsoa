import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-event-edit-create-dialog',
  templateUrl: './event-edit-create-dialog.component.html',
  styleUrls: ['./event-edit-create-dialog.component.scss']
})
export class EventEditCreateDialogComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<EventEditCreateDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
  }

  closeAndUpdate(event: any) {
    this.dialogRef.close(event);
  }

  closeDialog() {
    this.dialogRef.close();
  }

  get isEditing() {
    return this.event;
  }

  get event(): any {
    return this.data.event ? this.data.event : null;
  }

}
