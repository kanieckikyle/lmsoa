import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AuthenticationService } from 'src/app/accounts/services/authentication.service';

@Component({
  selector: 'app-event-card',
  templateUrl: './event-card.component.html',
  styleUrls: ['./event-card.component.scss']
})
export class EventCardComponent implements OnInit {

  @Input() calendarEvent: any;

  @Output() edit: EventEmitter<any> = new EventEmitter();

  constructor(private authService: AuthenticationService) { }

  ngOnInit() {
  }

  editEvent() {
    this.edit.emit(this.calendarEvent);
  }

  get canEdit() {
      return this.authService.currentUser &&
      (this.authService.currentUser.id === this.calendarEvent.organizer ||
      this.authService.currentUser.is_superuser);
  }

}
