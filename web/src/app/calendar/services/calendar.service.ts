import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable()
export class CalendarService {

  constructor(private http: HttpClient) {}

  getAllEvents() {
    return this.http
    .get(
      `${environment.base_url}/events/`,
      this.getJsonHeaders()
    );
  }

  getAllEventsBetweenDates(startDate: string, endDate: string) {
    return this.http
    .get(
      `${environment.base_url}/events/?start_date=${startDate}&end_date=${endDate}`,
      this.getJsonHeaders()
    );
  }

  getEvent(eventId: number) {
    return this.http
    .get(
      `${environment.base_url}/events/${eventId}/`,
      this.getJsonHeaders()
    );
  }

  createEvent(data: any) {
    return this.http
    .post(
      `${environment.base_url}/events/`,
      data,
      this.getJsonHeaders()
    );
  }

  editEvent(eventId: number, data: any) {
    return this.http
    .patch(
      `${environment.base_url}/events/${eventId}/`,
      data,
      this.getJsonHeaders()
    );
  }

  private getJsonHeaders() {
    return { headers: new HttpHeaders().set('Content-Type', 'application/json') };
  }

}
