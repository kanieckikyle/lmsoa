import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { CalendarModule as angularCalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';

import { CalendarRoutingModule } from './calendar-routing.module';
import { CalendarService } from './services/calendar.service';
import { CalendarPageComponent } from './pages/calendar-page/calendar-page.component';
import { BaseModule } from '../base/base.module';
import { FrequencyPipe } from './pipes/frequency.pipe';
import { EventCardComponent } from './elements/event-card/event-card.component';
import { CreateEventFormComponent } from './forms/create-event-form/create-event-form.component';
import { EventDetailDialogComponent } from './elements/event-detail-dialog/event-detail-dialog.component';
import { EventEditCreateDialogComponent } from './elements/event-edit-create-dialog/event-edit-create-dialog.component';

@NgModule({
  declarations: [
    CalendarPageComponent,
    FrequencyPipe,
    EventCardComponent,
    CreateEventFormComponent,
    EventDetailDialogComponent,
    EventEditCreateDialogComponent
  ],
  imports: [
    CommonModule,
    CalendarRoutingModule,
    angularCalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory
    }),
    BaseModule,
    ReactiveFormsModule
  ],
  providers: [
    CalendarService
  ],
  entryComponents: [
    EventDetailDialogComponent,
    EventEditCreateDialogComponent
  ]
})
export class CalendarModule { }
