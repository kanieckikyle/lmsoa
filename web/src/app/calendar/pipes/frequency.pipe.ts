import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'frequency'
})
export class FrequencyPipe implements PipeTransform {

  transform(value: number, args?: any): any {
    switch (value) {
      case 0:
        return 'yearly';
      case 1:
        return 'Monthly';
      case 2:
        return 'Weekly';
      case 3:
        return 'Daily';
      default:
        return 'Never';
    }
  }

}
