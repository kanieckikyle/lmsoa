import { Component, OnInit } from '@angular/core';
import { CalendarService } from '../../services/calendar.service';
import {
  CalendarEvent,
  CalendarMonthViewBeforeRenderEvent,
  CalendarWeekViewBeforeRenderEvent,
  CalendarDayViewBeforeRenderEvent,
} from 'angular-calendar';
import { ViewPeriod } from 'calendar-utils';
import * as moment from 'moment';
import { trigger, transition, style, animate, state } from '@angular/animations';
import { Subject } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { EventDetailDialogComponent } from '../../elements/event-detail-dialog/event-detail-dialog.component';
import { EventEditCreateDialogComponent } from '../../elements/event-edit-create-dialog/event-edit-create-dialog.component';
import { filter, switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-calendar-page',
  templateUrl: './calendar-page.component.html',
  styleUrls: ['./calendar-page.component.scss'],
  animations: [
    trigger('dayDetail', [
      state('collapsed', style({ height: '0px', overflow: 'hidden'})),
      state('expanded', style({ height: '*', overflow: 'auto'})),
      transition('collapsed <=> expanded', [
        animate('500ms cubic-bezier(0.4, 0.0, 0.2, 1)'),
      ])
    ])
  ]
})
export class CalendarPageComponent implements OnInit {

  viewDate: Date = new Date();
  viewPeriod: ViewPeriod;
  currentViewFormat: string = 'month';

  refresh: Subject<any> = new Subject();

  expandedDay: any;
  expandedEvents: any[] = [];

  events: CalendarEvent[] = [];

  constructor(private calendarService: CalendarService, public dialog: MatDialog) { }

  ngOnInit() { }

  createNewEvent() {
    const dialogRef = this.dialog.open(EventEditCreateDialogComponent, {
      minWidth: '50%',
      maxWidth: '90%',
      data: {}
    });
  }

  editEvent(event: any) {
    const dialogRef = this.dialog.open(EventEditCreateDialogComponent, {
        minWidth: '50%',
        maxWidth: '90%',
        data: {
          event: event
        }
      });

    dialogRef.afterClosed()
    .pipe(
      filter(Boolean)
    )
    .subscribe((result: any) => {
      this.events = this.events.filter(event => event.id !== result.id);
      this.events.push({
        ...result,
        start: new Date(result.start_date),
        end: new Date(result.end_date)
      });
      this.refresh.next();
    })
  }

  showDetails(event: any) {
    const dialogRef = this.dialog.open(EventDetailDialogComponent, {
      minWidth: '50%',
      maxWidth: '90%',
      data: event
    });

    dialogRef.beforeClose()
    .pipe(
      filter(Boolean),
      switchMap((event: any) => {
        return this.dialog.open(EventEditCreateDialogComponent, {
            minWidth: '50%',
            maxWidth: '90%',
            data: {
              event: event
            }
          })
          .afterClosed()
          .pipe(
            filter(Boolean)
          );
      }),
    )
    .subscribe((result: any) => {
      this.events = this.events.filter(event => event.id !== result.id);
      this.events.push({
        ...result,
        start: new Date(result.start_date),
        end: new Date(result.end_date)
      });
      this.refresh.next();
    });
  }

  dayClicked(event: any) {
    if (!moment(this.expandedDay).isSame(event.date)) {
      this.expandedDay = event.date;
      this.expandedEvents = event.events;
    } else {
      this.expandedDay = undefined;
      this.expandedEvents = [];
    }
  }

  changeFormat(event: {value: string}) {
    this.currentViewFormat = event.value;
  }

  changeViewDate(viewdate: string) {
    let multiplier: moment.unitOfTime.DurationConstructor;
    switch (this.currentViewFormat) {
      case 'month':
        multiplier = 'M';
        break;
      case 'day':
        multiplier = 'd';
        break;
      case 'week':
        multiplier = 'w';
        break;
      default:
        multiplier = 'M';
    }

    switch (viewdate) {
      case 'previous':
        this.viewDate = moment(this.viewDate).subtract(1, multiplier).toDate();
        break;
      case 'next':
        this.viewDate = moment(this.viewDate).add(1, multiplier).toDate();
        break;
      case 'today':
        this.viewDate = moment().toDate();
        break;
    }

    const start = moment(this.viewDate).startOf('isoWeek').isoWeekday(0).toDate();
    const end = moment(this.viewDate).endOf('isoWeek').isoWeekday(7).toDate();
    this.viewPeriod.end = end;
    this.viewPeriod.start = start;
    this.calendarService.getAllEventsBetweenDates(start.toISOString(), end.toISOString())
    .subscribe((events: any) => {
      this.events = events.map(event => ({
        id: event.id,
        start: new Date(event.start_date),
        end: event.end_date ? new Date(event.end_date) : null,
        title: event.title,
        description: event.description,
        frequency: event.frequency,
        organizer: event.organizer,
      }));
      this.refresh.next();
    });
  }

  getHeaderText(): string {
    switch (this.currentViewFormat) {
      case 'month':
        return moment(this.viewDate).format('MMMM YYYY');
      case 'week':
        const start = moment(this.viewDate).startOf('isoWeek').isoWeekday(0).format('MMM Do');
        const end = moment(this.viewDate).endOf('isoWeek').isoWeekday(7).format('MMM Do');
      return `${start} - ${end}, ${moment(this.viewDate).format('YYYY')}`;
      case 'day':
        return moment(this.viewDate).format('MMM Do YYYY');
    }
  }

  getEvents(viewRender: CalendarMonthViewBeforeRenderEvent | CalendarWeekViewBeforeRenderEvent | CalendarDayViewBeforeRenderEvent) {
    if (
      !this.viewPeriod ||
      !moment(this.viewPeriod.start).isSame(viewRender.period.start) ||
      !moment(this.viewPeriod.end).isSame(viewRender.period.end)
    ) {
      console.log('Updating view period');
      this.viewPeriod = viewRender.period;
      const startDate = viewRender.period.start.toISOString();
      const endDate = viewRender.period.end.toISOString();
      this.calendarService.getAllEventsBetweenDates(startDate, endDate)
        .subscribe((events: any[]) => {
          this.events = events.map(event => ({
            id: event.id,
            start: new Date(event.start_date),
            end: event.end_date ? new Date(event.end_date) : null,
            title: event.title,
            description: event.description,
            frequency: event.frequency,
            organizer: event.organizer,
          }));
        });
    }
  }
}
