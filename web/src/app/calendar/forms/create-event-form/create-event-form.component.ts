import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { GlobalService } from 'src/app/base/services/global.service';
import { CalendarService } from '../../services/calendar.service';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { AuthenticationService } from 'src/app/accounts/services/authentication.service';

@Component({
  selector: 'app-create-event-form',
  templateUrl: './create-event-form.component.html',
  styleUrls: ['./create-event-form.component.scss']
})
export class CreateEventFormComponent implements OnInit {

  @Input() event: any;

  isMobile = false;

  @Output() cancelled: EventEmitter<boolean> = new EventEmitter();
  @Output() submitted: EventEmitter<any> = new EventEmitter();

  form: FormGroup = new FormGroup({
    organizer: new FormControl(this.authService.currentUser.id),
    title: new FormControl('', [
      Validators.required,
    ]),
    description: new FormControl('', [
      Validators.required,
    ]),
    frequency: new FormControl('', [
      Validators.required
    ]),
    start_date: new FormControl('', [
      Validators.required
    ]),
    start_time: new FormControl('', [
      Validators.required
    ]),
    end_date: new FormControl('', [
      Validators.required
    ]),
    end_time: new FormControl('', [
      Validators.required
    ])
  });

  minDate = moment();

  constructor(
    private globalService: GlobalService,
    private calendarService: CalendarService,
    private authService: AuthenticationService
  ) { }

  ngOnInit() {
    if (this.event) {
      this.form.patchValue(this.event);
      const start = moment(this.event.start);
      const end = moment(this.event.end);
      this.form.patchValue({
        start_date: start,
        start_time: start.format('HH:mm a'),
        end_date: end,
        end_time: end.format('HH:mm a')
      });
    }

    this.globalService.screenSize$
    .subscribe((size: string) => {
      switch(size) {
        case 'x-small':
        case 'small':
          this.isMobile = true;
          break;
        default:
          this.isMobile = false;
          break;
      }
    });
  }

  submit() {
    if (!this.form.valid) {
      for (const key of Object.keys(this.form.controls)) {
        this.form.get(key).markAsTouched();
      }
      return;
    }
    const formData = this.form.getRawValue();
    formData['end_date'] = moment(`${this.end_date.value.format('M/D/YYYY')} ${this.end_time.value}`, 'M/D/YYYY HH:mm a').toISOString();
    formData['start_date'] = moment(`${this.start_date.value.format('M/D/YYYY')} ${this.start_time.value}`, 'M/D/YYYY HH:mm a').toISOString();
    delete formData['end_time'];
    delete formData['start_time'];

    let obs: Observable<any>;
    if (this.event) {
      obs = this.calendarService.editEvent(this.event.id, formData);
    } else {
      obs = this.calendarService.createEvent(formData);
    }
    obs.subscribe((event: any) => this.submitted.emit(event));
  }

  cancel() {
    this.cancelled.emit(true);
  }

  get title() {
    return this.form.get('title');
  }

  get description() {
    return this.form.get('description');
  }

  get frequency() {
    return this.form.get('frequency');
  }

  get start_date() {
    return this.form.get('start_date');
  }

  get start_time() {
    return this.form.get('start_time');
  }

  get end_date() {
    return this.form.get('end_date');
  }

  get end_time() {
    return this.form.get('end_time');
  }

}
