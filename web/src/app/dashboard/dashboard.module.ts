import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardOverviewPageComponent } from './pages/dashboard-overview-page/dashboard-overview-page.component';
import { BaseModule } from '../base/base.module';

@NgModule({
  declarations: [
    DashboardOverviewPageComponent
  ],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    BaseModule
  ]
})
export class DashboardModule { }
