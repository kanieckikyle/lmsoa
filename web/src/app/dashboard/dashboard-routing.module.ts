import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardOverviewPageComponent } from './pages/dashboard-overview-page/dashboard-overview-page.component';

const routes: Routes = [
  {
    path: '',
    component: DashboardOverviewPageComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
