import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  constructor(private http: HttpClient) { }

  uploadCsv(file: File) {
    const data = new FormData();
    data.append('csv', file);
    return this.http
      .post(
        `${environment.base_url}/owners/upload/bulk/`,
        data
      );
  }
}
