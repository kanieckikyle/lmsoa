import { Component, OnInit, ViewChild } from '@angular/core';
import { AdminService } from '../../services/admin.service';

@Component({
  selector: 'app-dashboard-overview-page',
  templateUrl: './dashboard-overview-page.component.html',
  styleUrls: ['./dashboard-overview-page.component.scss']
})
export class DashboardOverviewPageComponent implements OnInit {
  private csv: File;

  constructor(private adminService: AdminService) { }

  ngOnInit() {
  }

  ngAfterViewInit() {

  }

  submitCsv() {
    this.adminService.uploadCsv(this.csv)
    .subscribe((result: any) => {
      console.log(result);
    });
  }

  prepareFile(file: any) {
    if (file.target.files) {
      this.csv = file.target.files[0];
      console.log(this.csv);
    }
  }

}
