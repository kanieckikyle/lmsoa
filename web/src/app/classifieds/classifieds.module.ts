import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {NgxMaskModule} from 'ngx-mask';
import { ReactiveFormsModule } from '@angular/forms';

import { ClassifiedsRoutingModule } from './classifieds-routing.module';
import { ClassifiedComponent } from './elements/classified/classified.component';
import { BaseModule } from '../base/base.module';
import { ClassifiedsListPageComponent } from './pages/classifieds-list-page/classifieds-list-page.component';
import { CreateEditClassifiedFormComponent } from './forms/create-edit-classified-form/create-edit-classified-form.component';
import { CreateEditClassifiedPageComponent } from './pages/create-edit-classified-page/create-edit-classified-page.component';


@NgModule({
  declarations: [ClassifiedComponent, ClassifiedsListPageComponent, CreateEditClassifiedFormComponent, CreateEditClassifiedPageComponent],
  imports: [
    CommonModule,
    ClassifiedsRoutingModule,
    BaseModule,
    ReactiveFormsModule,
    NgxMaskModule.forRoot()
  ]
})
export class ClassifiedsModule { }
