import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AuthenticationService } from 'src/app/accounts/services/authentication.service';
import { MatDialog } from '@angular/material/dialog';
import { ClassifiedService } from '../../services/classified.service';
import { DeleteDialogComponent } from 'src/app/base/elements/delete-dialog/delete-dialog.component';
import { map, switchMap } from 'rxjs/operators';
import { of } from 'rxjs';

@Component({
  selector: 'app-classified',
  templateUrl: './classified.component.html',
  styleUrls: ['./classified.component.scss']
})
export class ClassifiedComponent implements OnInit {

  @Input() classified: any;
  @Input() showActions: boolean = true;

  @Output() deleted: EventEmitter<any> = new EventEmitter();

  constructor(private authService: AuthenticationService, public dialog: MatDialog, private classifiedService: ClassifiedService) { }

  ngOnInit() {}

  delete() {
    const dialogRef = this.dialog.open(DeleteDialogComponent, {
      data: {
        header: 'Delete classified listing?',
        body: `Are you sure you want to delete your classified listing?`
      }
    });

    dialogRef.afterClosed()
    .pipe(
      switchMap((confirmed: boolean) => {
        return confirmed ? this.classifiedService.deleteClassified(this.classified.id).pipe(map(_ => true)) : of(false)
      }),
    )
    .subscribe(result => {
      if (result) {
        this.deleted.emit(this.classified);
      }
    });
  }

  get isAuthor() {
    const user = this.authService.currentUser;
    return user && (user.id === this.classified.author.id || user.is_superuser);
  }

}
