import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClassifiedsListPageComponent } from './pages/classifieds-list-page/classifieds-list-page.component';
import { CreateEditClassifiedPageComponent } from './pages/create-edit-classified-page/create-edit-classified-page.component';

const routes: Routes = [
  {
    path: 'list',
    component: ClassifiedsListPageComponent
  },
  {
    path: 'create',
    component: CreateEditClassifiedPageComponent
  },
  {
    path: ':id/edit',
    component: CreateEditClassifiedPageComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClassifiedsRoutingModule { }
