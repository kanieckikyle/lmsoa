import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ClassifiedService } from '../../services/classified.service';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-create-edit-classified-form',
  templateUrl: './create-edit-classified-form.component.html',
  styleUrls: ['./create-edit-classified-form.component.scss']
})
export class CreateEditClassifiedFormComponent implements OnInit {

  form: FormGroup = new FormGroup({
    title: new FormControl('', [
      Validators.required
    ]),
    description: new FormControl('', [
      Validators.required
    ]),
    price: new FormControl('', [
      Validators.required,
      Validators.min(0)
    ]),
    public: new FormControl(false),
    type: new FormControl(''),
    lot_number: new FormControl('', [
      Validators.min(0)
    ])
  });

  attachments: File[] = [];
  attachmentUrls: string[] = [];
  @ViewChild('fileUploadInput') fileInput;

  @Input() classified: any;
  @Output() submitted: EventEmitter<any> = new EventEmitter();
  @Output() cancelled: EventEmitter<boolean> = new EventEmitter();

  constructor(private classifiedService: ClassifiedService) { }

  ngOnInit() {
    if (this.classified) {
      this.form.patchValue(this.classified);
    }
  }

  submit() {
    const formData = this.getClassifiedFormData();

    this.classifiedService.createNewClassified(formData)
    .pipe(
      switchMap((classified: any) => this.classifiedService.uploadAttachments(classified.id, this.getAttachmentsFormData()))
    )
    .subscribe((attachments: string[]) => this.submitted.emit(true));
  }

  cancel() {
    this.cancelled.emit(true);
  }

  getAttachments() {
    const fileList = (this.fileInput.nativeElement.files as FileList);
    for (let index = 0; index < fileList.length; index++) {
      const file = fileList.item(index);
      this.attachments.push(file);
    }
    this.getImagePreviews();
    this.fileInput.nativeElement.value = null;
  }

  getImagePreviews() {
    this.attachmentUrls = [];
    for (const file of this.attachments) {
      const reader = new FileReader();
      reader.onload = () => {
        this.attachmentUrls.push(reader.result as string);
      }
      reader.readAsDataURL(file);
    }
  }

  removeFile(index: number) {
    this.attachments.splice(index, 1);
    this.attachmentUrls.splice(index, 1);
  }

  private getClassifiedFormData() {
    const formData = new FormData();
    const formValue = this.form.getRawValue();
    Object.keys(formValue).forEach(key => formData.append(key, formValue[key]));
    return formData;
  }

  private getAttachmentsFormData() {
    const formData = new FormData();
    this.attachments.forEach((attachment, index) => {
      formData.append(`attachment${index}`, attachment);
    });
    return formData;
  }

  get title() {
    return this.form.get('title');
  }

  get description() {
    return this.form.get('description');
  }

  get price() {
    return this.form.get('price');
  }

  get public() {
    return this.form.get('public');
  }

  get type() {
    return this.form.get('type');
  }

  get lot_number() {
    return this.form.get('lot_number');
  }

}
