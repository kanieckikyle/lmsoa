import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateEditClassifiedFormComponent } from './create-edit-classified-form.component';

describe('CreateEditClassifiedFormComponent', () => {
  let component: CreateEditClassifiedFormComponent;
  let fixture: ComponentFixture<CreateEditClassifiedFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateEditClassifiedFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateEditClassifiedFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
