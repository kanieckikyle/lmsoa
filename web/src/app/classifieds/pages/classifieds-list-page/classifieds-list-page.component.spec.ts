import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClassifiedsListPageComponent } from './classifieds-list-page.component';

describe('ClassifiedsListPageComponent', () => {
  let component: ClassifiedsListPageComponent;
  let fixture: ComponentFixture<ClassifiedsListPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClassifiedsListPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClassifiedsListPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
