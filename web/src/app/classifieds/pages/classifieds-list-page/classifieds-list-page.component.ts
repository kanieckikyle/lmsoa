import { Component, OnInit } from '@angular/core';
import { ClassifiedService } from '../../services/classified.service';
import { AuthenticationService } from '../../../accounts/services/authentication.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-classifieds-list-page',
  templateUrl: './classifieds-list-page.component.html',
  styleUrls: ['./classifieds-list-page.component.scss']
})
export class ClassifiedsListPageComponent implements OnInit {

  classifieds: any[] = [];
  isLoggedIn: boolean = false;

  constructor(private classifiedService: ClassifiedService, private authService: AuthenticationService) { }

  ngOnInit() {
    this.authService.isLoggedIn$.subscribe((isLoggedIn: boolean) => this.isLoggedIn = isLoggedIn);
    this.classifiedService.getClassifieds()
    .subscribe((classifieds: any) => {
      this.classifieds = classifieds;
    });
  }

  removeClassified(classified: any) {
    this.classifieds = this.classifieds.filter(temp => temp.id !== classified.id);
  }

}
