import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateEditClassifiedPageComponent } from './create-edit-classified-page.component';

describe('CreateEditClassifiedPageComponent', () => {
  let component: CreateEditClassifiedPageComponent;
  let fixture: ComponentFixture<CreateEditClassifiedPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateEditClassifiedPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateEditClassifiedPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
