import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { map, switchMap } from 'rxjs/operators';
import { ClassifiedService } from '../../services/classified.service';
import { of } from 'rxjs';

@Component({
  selector: 'app-create-edit-classified-page',
  templateUrl: './create-edit-classified-page.component.html',
  styleUrls: ['./create-edit-classified-page.component.scss']
})
export class CreateEditClassifiedPageComponent implements OnInit {

  classified: any | null;

  constructor(private route: ActivatedRoute, private router: Router, private classifiedService: ClassifiedService) { }

  ngOnInit() {
    this.route.paramMap
    .pipe(
      map((params: ParamMap) => params.get('id')),
      switchMap((id: string | null) => {
        return id ? this.classifiedService.getClassified(id) : of(null);
      })
    )
    .subscribe((classified: any | null) => this.classified = classified);
  }

  goToListingDetail(listing: any) {
    this.router.navigate(['/classifieds', 'list']);
  }

  goToListingList() {
    this.router.navigate(['/classifieds', 'list']);
  }

}
