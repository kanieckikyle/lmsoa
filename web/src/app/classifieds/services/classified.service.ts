import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ClassifiedService {

  constructor(private http: HttpClient) {}

  getClassifieds(params: string = '') {
      return this.http
      .get(
          `${environment.base_url}/classifieds/${params}`,
          this.getJsonHeaders()
      );
  }

  getClassified(id: string) {
    return this.http
    .get(
      `${environment.base_url}/classifieds/${id}/`,
      this.getJsonHeaders()
    );
  }

  createNewClassified(data: FormData) {
    return this.http
    .post(
      `${environment.base_url}/classifieds/`,
      data
    );
  }

  uploadAttachments(id: string, data: FormData) {
    return this.http
    .post(
      `${environment.base_url}/classifieds/${id}/attachments/`,
      data
    );
  }

  deleteClassified(id: string) {
    return this.http
    .delete(
      `${environment.base_url}/classifieds/${id}/`,
      this.getJsonHeaders()
    );
  }

  private getJsonHeaders(): any {
      return { headers: new HttpHeaders().set('Content-Type', 'application/json') };
  }
}
