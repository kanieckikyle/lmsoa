import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DuesListPageComponent } from './pages/dues-list-page/dues-list-page.component';

const routes: Routes = [
  {
    path: '',
    component: DuesListPageComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DuesRoutingModule { }
