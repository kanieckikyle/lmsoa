import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DuesRoutingModule } from './dues-routing.module';
import { DuesListPageComponent } from './pages/dues-list-page/dues-list-page.component';
import { BaseModule } from '../base/base.module';

@NgModule({
  declarations: [DuesListPageComponent],
  imports: [
    CommonModule,
    DuesRoutingModule,
    BaseModule
  ]
})
export class DuesModule { }
