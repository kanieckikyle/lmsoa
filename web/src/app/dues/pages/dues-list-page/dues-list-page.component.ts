import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-dues-list-page',
  templateUrl: './dues-list-page.component.html',
  styleUrls: ['./dues-list-page.component.scss']
})
export class DuesListPageComponent implements OnInit {

  displayedColumns: string[] = ['month', 'paidDate', 'status'];
  duesDatasource: MatTableDataSource<any> = new MatTableDataSource();

  dues: any[] = [
    {
      month: 'December',
      date_paid: 'December 18th',
      status: 'accepted'
    },
    {
      month: 'January',
      date_paid: 'January 3rd',
      status: 'accepted'
    },
    {
      month: 'February',
      date_paid: 'February 1st',
      status: 'accepted'
    }
  ];

  constructor() { }

  ngOnInit() {
    this.duesDatasource.data = this.dues;
  }

  getStatusColor(status: string): string {
    switch (status) {
      case 'accepted':
        return 'green';
      case 'pending':
        return 'goldenrod';
      default:
        return 'red';
    }
  }

}
