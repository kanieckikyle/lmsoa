import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DuesListPageComponent } from './dues-list-page.component';

describe('DuesListPageComponent', () => {
  let component: DuesListPageComponent;
  let fixture: ComponentFixture<DuesListPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DuesListPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DuesListPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
