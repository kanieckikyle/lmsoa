import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MessageBoardService {

  constructor(private http: HttpClient) { }

  getMessages(params: string = '') {
    return this.http
    .get(
      `${environment.base_url}/messages/${params}`,
      this.getJsonHeaders()
    );
  }

  getMessage(id: number) {
    return this.http
    .get(
      `${environment.base_url}/messages/${id}/`,
      this.getJsonHeaders()
    );
  }

  createNewMessage(data: any) {
    return this.http
    .post(
      `${environment.base_url}/messages/`,
      data,
      this.getJsonHeaders()
    );
  }

  updateMessage(id: number, data: any) {
    return this.http
    .patch(
      `${environment.base_url}/messages/${id}/`,
      data,
      this.getJsonHeaders()
    );
  }

  deleteMessage(id: number) {
    return this.http
    .delete(`${environment.base_url}/messages/${id}/`);
  }

  createNewComment(message_id: number, data: any) {
    return this.http
    .post(
      `${environment.base_url}/messages/${message_id}/comments/`,
      data,
      this.getJsonHeaders()
    );
  }

  private getJsonHeaders() {
    return { headers: new HttpHeaders().set('Content-Type', 'application/json') };
  }
}
