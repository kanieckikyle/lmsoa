import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { MessageBoardService } from '../../services/message-board.service';
import { switchMap } from 'rxjs/operators';
import { FormControl, Validators } from '@angular/forms';
import { AuthenticationService } from 'src/app/accounts/services/authentication.service';

@Component({
  selector: 'app-message-detail-page',
  templateUrl: './message-detail-page.component.html',
  styleUrls: ['./message-detail-page.component.scss']
})
export class MessageDetailPageComponent implements OnInit {

  message: any;

  commentControl = new FormControl('', [
    Validators.required
  ]);

  constructor(private route: ActivatedRoute, private messageService: MessageBoardService, private authService: AuthenticationService) { }

  ngOnInit() {
    this.route.paramMap
    .pipe(
      switchMap((params: ParamMap) => this.messageService.getMessage(+params.get('id')))
    )
    .subscribe((message: any) => this.message = message);
  }

  submitNewComment() {
    this.messageService.createNewComment(this.message.id, {body: this.commentControl.value})
    .pipe(
      switchMap((_: any) => this.messageService.getMessage(this.message.id))
    )
    .subscribe((message: any) => {
      this.message = message;
      this.commentControl.reset('');
    });
  }

}
