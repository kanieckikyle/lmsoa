import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { MessageBoardService } from '../../services/message-board.service';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-edit-message-page',
  templateUrl: './edit-message-page.component.html',
  styleUrls: ['./edit-message-page.component.scss']
})
export class EditMessagePageComponent implements OnInit {

  post: any;

  constructor(private router: Router, private messageService: MessageBoardService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.paramMap
    .pipe(
      switchMap((params: ParamMap) => this.messageService.getMessage(+params.get('id')))
    )
    .subscribe((post: any) => {
      this.post = post;
    });
  }

  goToMessageDetails(message: any) {
    this.router.navigate(['/messages', message.id]);
  }

  goToMessageList() {
    this.router.navigate(['/messages', 'list']);
  }

}
