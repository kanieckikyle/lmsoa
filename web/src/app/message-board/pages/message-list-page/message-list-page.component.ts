import { Component, OnInit, ViewChild, ChangeDetectionStrategy } from '@angular/core';
import { MessageBoardService } from '../../services/message-board.service';
import { tap, throttleTime, mergeMap, map, scan } from 'rxjs/operators';
import { BehaviorSubject, Observable } from 'rxjs';
import { CdkVirtualScrollViewport } from '@angular/cdk/scrolling';

@Component({
  selector: 'app-message-list-page',
  templateUrl: './message-list-page.component.html',
  styleUrls: ['./message-list-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MessageListPageComponent implements OnInit {

  messages: any[] = [];
  private offset = new BehaviorSubject(1);

  private _isLoading = new BehaviorSubject(false);
  noMore = false;
  lastScrollPos = 0;

  @ViewChild(CdkVirtualScrollViewport) postsContainer: CdkVirtualScrollViewport;

  constructor(private messageService: MessageBoardService) { }

  ngOnInit() {
    this.offset.pipe(
      mergeMap(n => this.getMoreMessages()),
      scan((a, c) => a.concat(c), []),
      tap(a => {
        this._isLoading.next(false);
      })
    )
    .subscribe((messages: any[]) => {
      this.messages = messages.sort((a: any, b: any) => {
        if (a.pinned && !b.pinned) {
          return -1;
        } else if (b.pinned && !a.pinned) {
          return 1;
        }
        return new Date(b.timestamp).getTime() - new Date(a.timestamp).getTime();
      });
    });
  }

  appendMoreMessages(scrollEvent) {
    if (this.noMore) {
      return;
    }

    const end = this.postsContainer.getRenderedRange().end;
    const total = this.postsContainer.getDataLength();
    if (end === total) {
      this.offset.next(this.offset.value + 1);
    }
  }

  deleteMessageBoardPost(message: any) {
    this.messages = this.messages.filter(msg => msg.id !== message.id)
  }

  private getMoreMessages(): Observable<any> {
    this._isLoading.next(true);
    return this.messageService.getMessages(`?page=${this.offset.value}`)
    .pipe(
      tap((data: any) => data.items.length ? null : this.noMore = true),
      map((data: any) => data.items),
      tap(_ => this._isLoading.next(false))
    );
  }

  trackByIdx(i) {
    return i;
  }

  get isLoading$(): Observable<boolean> {
    return this._isLoading.asObservable();
  }

}
