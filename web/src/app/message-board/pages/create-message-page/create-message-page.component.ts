import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-message-page',
  templateUrl: './create-message-page.component.html',
  styleUrls: ['./create-message-page.component.scss']
})
export class CreateMessagePageComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  goToMessageDetails(message: any) {
    this.router.navigate(['/messages', message.id]);
  }

  goToMessageList() {
    this.router.navigate(['/messages', 'list']);
  }

}
