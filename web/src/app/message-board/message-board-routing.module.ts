import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MessageListPageComponent } from './pages/message-list-page/message-list-page.component';
import { CreateMessagePageComponent } from './pages/create-message-page/create-message-page.component';
import { EditMessagePageComponent } from './pages/edit-message-page/edit-message-page.component';
import { MessageDetailPageComponent } from './pages/message-detail-page/message-detail-page.component';

const routes: Routes = [
  {
    path: 'list',
    component: MessageListPageComponent
  },
  {
    path: 'create',
    component: CreateMessagePageComponent
  },
  {
    path: ':id',
    component: MessageDetailPageComponent
  },
  {
    path: ':id/edit',
    component: EditMessagePageComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MessageBoardRoutingModule { }
