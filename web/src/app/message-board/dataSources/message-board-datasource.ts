import { GenericDataSource } from '../../base/dataSources/generic-datasource';
import { MessageBoardService } from '../services/message-board.service';
import { switchMap } from 'rxjs/operators';


export class MessageBoardDataSource extends GenericDataSource<any> {

    _title: string;
    _pinned: boolean;

    constructor(
        private messageService: MessageBoardService,
        {
        title = null,
        pinned = null,
        page = 0,
        pageLength = 10
        } = {}
    ) {
        super({page: page, pageLength: pageLength});
        this._title = title;
        this._pinned = pinned;
        this.update();
    }

    update() {
        this._loading.next(true);
        this.messageService.getMessages(this._filterPredicate())
        .subscribe((messages: any) => {
            this.count += messages.count;
            this.page_length += messages.page_length;
            this.updatePaginator();
            const data = (messages.items as any[]);
            data.sort((a: any, b: any) => {
                if (a.pinned && !b.pinned) {
                    return -1;
                } else if (!a.pinned && b.pinned) {
                    return 1;
                }
                return 0;
            });
            this._renderData.next(data);
            this._loading.next(false);
        });
    }

    set title(title: string | null) {
        this._title = title;
        this.updatePaginator();
    }

    set pinned(pinned: boolean | null) {
        this._pinned = pinned;
        this.updatePaginator();
    }
}
