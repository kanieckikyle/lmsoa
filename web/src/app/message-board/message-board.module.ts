import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { MessageBoardRoutingModule } from './message-board-routing.module';
import { MessageListPageComponent } from './pages/message-list-page/message-list-page.component';
import { MessageCreateEditFormComponent } from './forms/message-create-edit-form/message-create-edit-form.component';
import { BaseModule } from '../base/base.module';
import { CreateMessagePageComponent } from './pages/create-message-page/create-message-page.component';
import { EditMessagePageComponent } from './pages/edit-message-page/edit-message-page.component';
import { MessageDetailPageComponent } from './pages/message-detail-page/message-detail-page.component';
import { MessageBoardPostComponent } from './elements/message-board-post/message-board-post.component';

@NgModule({
  declarations: [
    MessageListPageComponent,
    MessageCreateEditFormComponent,
    CreateMessagePageComponent,
    EditMessagePageComponent,
    MessageDetailPageComponent,
    MessageBoardPostComponent
  ],
  imports: [
    CommonModule,
    MessageBoardRoutingModule,
    BaseModule,
    ReactiveFormsModule
  ]
})
export class MessageBoardModule { }
