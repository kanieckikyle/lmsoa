import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MessageCreateEditFormComponent } from './message-create-edit-form.component';

describe('MessageCreateEditFormComponent', () => {
  let component: MessageCreateEditFormComponent;
  let fixture: ComponentFixture<MessageCreateEditFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MessageCreateEditFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MessageCreateEditFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
