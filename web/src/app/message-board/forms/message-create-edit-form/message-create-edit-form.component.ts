import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MessageBoardService } from '../../services/message-board.service';
import { AuthenticationService } from 'src/app/accounts/services/authentication.service';

@Component({
  selector: 'app-message-create-edit-form',
  templateUrl: './message-create-edit-form.component.html',
  styleUrls: ['./message-create-edit-form.component.scss']
})
export class MessageCreateEditFormComponent implements OnInit {

  form: FormGroup = new FormGroup({
    title: new FormControl('', [
      Validators.required,
      Validators.minLength(5),
      Validators.maxLength(50)
    ]),
    body: new FormControl('', [
      Validators.required,
    ]),
    pinned: new FormControl(false)
  });

  @Input() post: any;

  @Output() submitted: EventEmitter<any> = new EventEmitter();
  @Output() cancelled: EventEmitter<boolean> = new EventEmitter();

  constructor(private messageService: MessageBoardService, private authService: AuthenticationService) { }

  ngOnInit() {
    if (this.post) {
      this.form.patchValue(this.post);
    }
  }

  submit() {
    if (!this.form.valid) {
      Object.keys(this.form.controls).forEach(field => { // {1}
        const control = this.form.get(field);            // {2}
        control.markAsTouched({ onlySelf: true });       // {3}
      });
      return;
    }

    if (this.post) {
      this.messageService.updateMessage(this.post.id, this.form.getRawValue())
      .subscribe((message: any) => this.submitted.emit(message));
    } else {
      this.messageService.createNewMessage(this.form.getRawValue())
      .subscribe((message: any) => this.submitted.emit(message));
    }
  }

  cancelMessage() {
    this.cancelled.emit(true);
  }

  get isSuperuser() {
    return this.authService.currentUser && this.authService.currentUser.is_superuser;
  }

  get title() {
    return this.form.get('title');
  }

  get body() {
    return this.form.get('body');
  }

  get pinned() {
    return this.form.get('pinned');
  }

}
