import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AuthenticationService } from 'src/app/accounts/services/authentication.service';
import { MatDialog } from '@angular/material/dialog';
import { DeleteDialogComponent } from 'src/app/base/elements/delete-dialog/delete-dialog.component';
import { MessageBoardService } from '../../services/message-board.service';
import { switchMap, map } from 'rxjs/operators';
import { of } from 'rxjs';

@Component({
  selector: 'app-message-board-post',
  templateUrl: './message-board-post.component.html',
  styleUrls: ['./message-board-post.component.scss']
})
export class MessageBoardPostComponent implements OnInit {

  @Input() message: any;
  @Input() showCommentButton = true;
  @Input() showButtons = true;
  @Input() restrictHeight = true;

  @Output() deleted: EventEmitter<boolean> = new EventEmitter();

  constructor(
    private authService: AuthenticationService,
    public dialog: MatDialog,
    private messageBoardService: MessageBoardService
  ) { }

  ngOnInit() {
  }

  delete() {
    const dialogRef = this.dialog.open(DeleteDialogComponent, {
      data: {
        header: 'Delete message board post?',
        body: `Are you sure you want to delete your message board post?`
      }
    });

    dialogRef.afterClosed()
    .pipe(
      switchMap((confirmed: boolean) => {
        return confirmed ? this.messageBoardService.deleteMessage(this.message.id).pipe(map(_ => true)) : of(false)
      }),
    )
    .subscribe(result => {
      if (result) {
        this.deleted.emit(this.message);
      }
    });
  }

  get isAuthor(): boolean {
    const user = this.authService.currentUser;
    return user !== null && (user.id === this.message.author.id || user.is_superuser);
  }

}
