import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MessageBoardPostComponent } from './message-board-post.component';

describe('MessageBoardPostComponent', () => {
  let component: MessageBoardPostComponent;
  let fixture: ComponentFixture<MessageBoardPostComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MessageBoardPostComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MessageBoardPostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
